package formatter

import (
	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/helper"
)

type PortofolioFormatter struct {
	ID               int                        `json:"id"`
	UserID           int                        `json:"user_id"`
	Title            string                     `json:"title"`
	Deskripsi        string                     `json:"deskripsi"`
	Status           string                     `json:"status"`
	ImageCover       string                     `json:"image_cover"`
	Kategori         string                     `json:"kategori"`
	Tarif            string                     `json:"tarif"`
	PortofolioImages []PortofolioImageFormatter `json:"portofolio_images"`
}

type PortofolioImageFormatter struct {
	ID           int    `json:"id"`
	PortofolioID int    `json:"portofolio_id"`
	Image        string `json:"image"`
	Deskripsi    string `json:"deskripsi"`
	IsPrimary    int    `json:"is_primary"`
}

func FormatPortofolio(portofolio entity.Portofolio) PortofolioFormatter {
	portofolioFormatter := PortofolioFormatter{}
	portofolioFormatter.ID = portofolio.ID
	portofolioFormatter.UserID = portofolio.UserID
	portofolioFormatter.Title = portofolio.Title
	portofolioFormatter.Deskripsi = portofolio.Deskripsi
	portofolioFormatter.Status = portofolio.Status
	portofolioFormatter.Kategori = portofolio.Kategori
	portofolioFormatter.Tarif = helper.FormatNumber(portofolio.Tarif)
	portofolioFormatter.ImageCover = ""

	if len(portofolio.PortofolioImages) > 0 {
		isPrimaryImage := ""
		for _, isImage := range portofolio.PortofolioImages {
			pImage := isImage.IsPrimary
			if pImage == 1 {
				isPrimaryImage = isImage.Image
			} else {
				isPrimaryImage = ""
			}
		}
		portofolioFormatter.ImageCover = isPrimaryImage
	}

	images := []PortofolioImageFormatter{}
	for _, imagePortofolio := range portofolio.PortofolioImages {
		portofolioImageFormatter := PortofolioImageFormatter{}
		portofolioImageFormatter.ID = imagePortofolio.ID
		portofolioImageFormatter.PortofolioID = imagePortofolio.PortofolioID
		portofolioImageFormatter.Image = imagePortofolio.Image
		portofolioImageFormatter.Deskripsi = imagePortofolio.Deskripsi
		portofolioImageFormatter.IsPrimary = imagePortofolio.IsPrimary
		images = append(images, portofolioImageFormatter)
	}
	portofolioFormatter.PortofolioImages = images
	return portofolioFormatter
}
func FormatPortofolios(portofolios []entity.Portofolio) []PortofolioFormatter {
	portofoliosFormatter := []PortofolioFormatter{}
	for _, portofolio := range portofolios {
		portofolioFormatter := FormatPortofolio(portofolio)
		portofoliosFormatter = append(portofoliosFormatter, portofolioFormatter)
	}
	return portofoliosFormatter
}

func FormatPortofolioImage(portofolioImage entity.PortofolioImage) PortofolioImageFormatter {
	portofolioImagesFormatter := PortofolioImageFormatter{}
	portofolioImagesFormatter.ID = portofolioImage.ID
	portofolioImagesFormatter.PortofolioID = portofolioImage.PortofolioID
	portofolioImagesFormatter.Image = portofolioImage.Image
	portofolioImagesFormatter.Deskripsi = portofolioImage.Deskripsi
	return portofolioImagesFormatter
}

func FormatPortofolioImages(portofolioImages []entity.PortofolioImage) []PortofolioImageFormatter {
	portofolioImagesFormatter := []PortofolioImageFormatter{}
	for _, portofolioImage := range portofolioImages {
		portofolioImageFormatter := FormatPortofolioImage(portofolioImage)
		portofolioImagesFormatter = append(portofolioImagesFormatter, portofolioImageFormatter)
	}

	return portofolioImagesFormatter
}

//Generated by Micagen at 21 Desember 2021
