package entity

import "time"

type Review struct {
	ID        int
	UserID    int
	Rating    int
	NoProject string
	Ulasan    string
	CreatedAt time.Time
	UpdatedAt time.Time
}
