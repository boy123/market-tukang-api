package entity

import "time"

type Slider struct {
	ID        int
	Image     string
	Active    string
	CreatedAt time.Time
	UpdatedAt time.Time
}
