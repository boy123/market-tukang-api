package entity

import (
	"time"
)

type MasterBiodata struct {
	ID              int
	UserID          int
	DeskripsiProfil string
	Alamat          string
	JenisKelamin    string
	NikKTP          string
	ImageKTP        string
	Skill           string
	RatePerDays     float64
	ActiveWork      string
	JamKerja        string
	CreatedAt       time.Time
	UpdatedAt       time.Time
	MasterUser      MasterUser `gorm:"foreignKey:UserID;references:ID"`
}
