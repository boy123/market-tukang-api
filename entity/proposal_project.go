package entity

import "time"

type ProposalProject struct {
	ID         int
	NoProject  string
	UserID     int
	IsInvited  string
	Status     string
	CreatedAt  time.Time
	UpdatedAt  time.Time
	MasterUser MasterUser `gorm:"foreignKey:UserID;references:ID"`
}
