package entity

import "time"

type Project struct {
	ID               int
	NoProject        string
	UserID           int
	Judul            string
	Deskripsi        string
	Lokasi           string
	TargetSelesai    string
	IDJenisPekerjaan int
	EstimasiBudget   float64
	Status           int
	CreatedAt        time.Time
	UpdatedAt        time.Time
	ProposalProject  []ProposalProject `gorm:"foreignKey:NoProject;references:NoProject"`
	MasterUser       MasterUser        `gorm:"foreignKey:UserID;references:ID"`
}
