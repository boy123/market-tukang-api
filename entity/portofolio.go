package entity

import "time"

type Portofolio struct {
	ID               int
	UserID           int
	Title            string
	Deskripsi        string
	Status           string
	Kategori         string
	Tarif            int
	PortofolioImages []PortofolioImage `gorm:"foreignKey:PortofolioID;references:ID"`
	MasterUser       MasterUser        `gorm:"foreignKey:UserID;references:ID"`
	CreatedAt        time.Time
	UpdateAt         time.Time
}

type PortofolioImage struct {
	ID           int
	PortofolioID int
	Image        string
	IsPrimary    int
	Deskripsi    string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
