package entity

import "time"

type ErrorLog struct {
	ErrorMessage string    `firestore:"error_message"`
	Time         time.Time `firestore:"time"`
}
