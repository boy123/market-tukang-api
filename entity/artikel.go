package entity

import "time"

type Artikel struct {
	ID         int
	Title      string
	Image      string
	Content    string
	ViewsCount int
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
