package entity

import "time"

type KategoriPekerjaan struct {
	ID        int
	Kategori  string
	CreatedAt time.Time
	UpdatedAt time.Time
}
