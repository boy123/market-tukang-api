package entity

import "time"

type Barang struct {
	ID              int
	IDToko          int
	NamaBarang      string
	DeskripsiBarang string
	FotoBarang      string
	Harga           float64
	Satuan          string
	Tersedia        int
	CreatedAt       time.Time
	UpdatedAt       time.Time
	Toko            Toko `gorm:"foreignKey:IDToko;references:ID"`
}
