package entity

import (
	"time"
)

type MasterUser struct {
	ID              int
	Username        string
	NamaLengkap     string `json:"nama_lengkap"`
	Password        string
	Email           string
	PhoneNumber     string `json:"phone_number"`
	Role            string
	Status          int
	TokenFCM        string
	ProfilePicture  string `json:"profile_picture"`
	ActiveAt        time.Time
	DateUntilActive time.Time
	AlasanNonAktif  string
	SaldoKoin       int
	Badge           string
	Verified        int
	CreatedAt       time.Time
	UpdatedAt       time.Time
	Latitude        float64
	Longitude       float64
}
type User struct {
	ID              int
	Username        string
	NamaLengkap     string
	Password        string
	Email           string
	PhoneNumber     string
	Role            string
	Status          int
	TokenFCM        string
	ProfilePicture  string
	ActiveAt        time.Time
	DateUntilActive time.Time
	AlasanNonAktif  string
	SaldoKoin       int
	Badge           string
	Verified        int
	CreatedAt       time.Time
	UpdatedAt       time.Time
}
