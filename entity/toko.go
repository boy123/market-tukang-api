package entity

import "time"

type Toko struct {
	ID              int
	UserID          int
	NamaToko        string
	Alamat          string
	NoTelp          string
	HariOperasional string
	JamOperasional  string
	Status          string
	CreatedAt       time.Time
	UpdatedAt       time.Time
	MasterUser      MasterUser `gorm:"foreignKey:UserID;references:ID"`
}
