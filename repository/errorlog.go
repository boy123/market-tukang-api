package repository

import (
	"context"
	"fmt"
	"runtime"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"google.golang.org/api/iterator"
)

type ErrorLogRepository interface {
	GetAll(search string, page, size int) ([]map[string]interface{}, error)
}

type errorLogRepository struct {
	app *firebase.App
}

func NewErrorLogRepository(app *firebase.App) *errorLogRepository {
	return &errorLogRepository{app}
}

func (r *errorLogRepository) GetAll(search string, page, size int) ([]map[string]interface{}, error) {
	ctx := context.Background()
	searchQuery := search

	client, err := r.app.Firestore(ctx)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
	}
	collections := client.Collection("errorLog")
	var value []map[string]interface{}
	if searchQuery != "" {
		fetchData := collections.OrderBy("time", firestore.Asc)
		iter := fetchData.Documents(ctx)
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			value = append(value, doc.Data())
		}
	} else {
		fetchData := collections.OrderBy("time", firestore.Asc)
		iter := fetchData.Documents(ctx)
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}

			value = append(value, doc.Data())
		}

	}
	return value, nil
}
