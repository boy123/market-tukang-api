package helper

import (
	"gopkg.in/gomail.v2"
)

type config struct {
	CONFIG_SMTP_HOST     string
	CONFIG_SMTP_PORT     int
	CONFIG_SENDER_NAME   string
	CONFIG_AUTH_EMAIL    string
	CONFIG_AUTH_PASSWORD string
	CONFIG_RECEPIENT     string
	CONFIG_SUBJECT       string
}

func SendMail(CONFIG_SMTP_HOST string, CONFIG_SMTP_PORT int, CONFIG_SENDER_NAME string, CONFIG_AUTH_EMAIL string, CONFIG_AUTH_PASSWORD string, CONFIG_RECEPIENT string, CONFIG_SUBJECT string) (string, error) {
	var configuration config
	configuration.CONFIG_SMTP_HOST = CONFIG_SMTP_HOST
	configuration.CONFIG_SMTP_PORT = CONFIG_SMTP_PORT
	configuration.CONFIG_SENDER_NAME = CONFIG_SENDER_NAME
	configuration.CONFIG_AUTH_EMAIL = CONFIG_AUTH_EMAIL
	configuration.CONFIG_AUTH_PASSWORD = CONFIG_AUTH_PASSWORD
	configuration.CONFIG_RECEPIENT = CONFIG_RECEPIENT
	configuration.CONFIG_SUBJECT = CONFIG_SUBJECT
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", configuration.CONFIG_SENDER_NAME)
	mailer.SetHeader("To", configuration.CONFIG_RECEPIENT)
	mailer.SetHeader("Subject", configuration.CONFIG_SUBJECT)
	mailer.SetBody("text/html", "Hello, Please verify your email account on this link <a href="+CONFIG_SENDER_NAME+">"+CONFIG_SENDER_NAME+"</a>")

	dialer := gomail.NewDialer(
		configuration.CONFIG_SMTP_HOST,
		configuration.CONFIG_SMTP_PORT,
		configuration.CONFIG_AUTH_EMAIL,
		configuration.CONFIG_AUTH_PASSWORD,
	)

	err := dialer.DialAndSend(mailer)
	if err != nil {
		return "", err
	}
	return "success", nil
}
