package helper

import (
	"github.com/leekchan/accounting"
)

func FormatNumber(nominal int) string {
	ac := accounting.Accounting{Symbol: "Rp.", Precision: 0, Thousand: ".", Decimal: ","}
	converted := ac.FormatMoney(nominal) // "€4.999,99"
	return converted
}
