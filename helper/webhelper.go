package helper

type WebResponse struct {
	TotalItems   int         `json:"totalItems"`
	Data         interface{} `json:"data"`
	TotalPages   int         `json:"totalPages"`
	CurrentPages int         `json:"currrentPages"`
}

type Data struct {
	Number int    `json:"no"`
	Name   string `json:"name"`
	Age    int    `json:"age"`
}

func WebApiResponse(recordsFiltered int, totalPages int, currentPages int, data interface{}) WebResponse {
	jsonResponse := WebResponse{
		TotalItems:   recordsFiltered,
		Data:         data,
		TotalPages:   totalPages,
		CurrentPages: currentPages,
	}
	return jsonResponse
}
