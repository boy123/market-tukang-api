package helper

func Truncate(words string, max int) string {
	if len(words) >= max {
		return words[:max] + "..."
	}
	return words
}
