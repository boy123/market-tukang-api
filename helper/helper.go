package helper

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime"
	"time"

	firebase "firebase.google.com/go"
	"github.com/go-playground/validator/v10"
	"github.com/joho/godotenv"
	"google.golang.org/api/option"
)

type Response struct {
	ApiVersion string      `json:"api_version"`
	Meta       Meta        `json:"meta"`
	Data       interface{} `json:"data"`
}
type Meta struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Status  string `json:"status"`
}

func ApiResponse(message string, code int, status string, data interface{}) Response {
	meta := Meta{
		Message: message,
		Code:    code,
		Status:  status,
	}
	jsonResponse := Response{
		ApiVersion: os.Getenv("API_VERSION"),
		Meta:       meta,
		Data:       data,
	}
	return jsonResponse
}
func FormatValidationError(err error) []string {
	fmt.Println(err.Error())
	var errors []string
	for _, e := range err.(validator.ValidationErrors) {
		errors = append(errors, e.Error())
	}
	return errors
}

var iv = []byte{34, 35, 35, 57, 68, 4, 35, 36, 7, 8, 35, 23, 35, 86, 35, 23}

func encodeBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func decodeBase64(s string) []byte {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		log.Panic(err)
	}
	return data
}

func Encrypt(text string) string {
	env := godotenv.Load()
	if env != nil {
		log.Fatal("Error on loading .env file")
	}
	key := os.Getenv("ENC_KEY")
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		panic(err)
	}
	plaintext := []byte(text)
	cfb := cipher.NewCFBEncrypter(block, iv)
	ciphertext := make([]byte, len(plaintext))
	cfb.XORKeyStream(ciphertext, plaintext)
	return encodeBase64(ciphertext)
}

func Decrypt(text string) string {
	env := godotenv.Load()
	if env != nil {
		log.Fatal("Error on loading .env file")
	}
	key := os.Getenv("ENC_KEY")
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		panic(err)
	}
	ciphertext := decodeBase64(text)
	cfb := cipher.NewCFBEncrypter(block, iv)
	plaintext := make([]byte, len(ciphertext))
	cfb.XORKeyStream(plaintext, ciphertext)
	return string(plaintext)
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letters = []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandStringRunes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

var Logger *log.Logger

func CreateLog(err error, message string) {
	ctx := context.Background()
	sa := option.WithCredentialsFile(os.Getenv("CREDENTIAL_JSON"))
	app, errno := firebase.NewApp(ctx, nil, sa)
	if errno != nil {
		fmt.Println(errno)
	}
	client, errnp := app.Firestore(ctx)
	if errnp != nil {
		fmt.Println(errno)
	}
	defer client.Close()

	if err != nil {

		_, _, _ = client.Collection("error_log").Add(ctx, map[string]interface{}{
			"time":          time.Now(),
			"error_message": message,
		})
	}
}

func PrintErr(err error, message string) {
	CreateLog(err, message)
}

func HandleError(err error) (b bool) {
	if err != nil {
		_, fn, line, _ := runtime.Caller(0)
		log.Printf("[error] %s:%d %v", fn, line, err)
		b = true
	}
	return
}

func FancyHandleError(err error) (b bool) {
	if err != nil {
		// notice that we're using 1, so it will actually log the where
		// the error happened, 0 = this function, we don't want that.
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		fmt.Println(errornya)
		b = true
	}
	return
}
