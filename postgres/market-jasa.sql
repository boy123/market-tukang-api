--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: artikels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.artikels (
    id bigint NOT NULL,
    title text,
    image text,
    content text,
    views_count bigint,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.artikels OWNER TO postgres;

--
-- Name: artikels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.artikels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.artikels_id_seq OWNER TO postgres;

--
-- Name: artikels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.artikels_id_seq OWNED BY public.artikels.id;


--
-- Name: barangs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barangs (
    id bigint NOT NULL,
    id_toko bigint,
    nama_barang text,
    deskripsi_barang text,
    foto_barang text,
    harga numeric,
    satuan text,
    tersedia bigint,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.barangs OWNER TO postgres;

--
-- Name: barangs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barangs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barangs_id_seq OWNER TO postgres;

--
-- Name: barangs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barangs_id_seq OWNED BY public.barangs.id;


--
-- Name: jenis_pekerjaans; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jenis_pekerjaans (
    id bigint NOT NULL,
    jenis_pekerjaan text
);


ALTER TABLE public.jenis_pekerjaans OWNER TO postgres;

--
-- Name: jenis_pekerjaans_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jenis_pekerjaans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_pekerjaans_id_seq OWNER TO postgres;

--
-- Name: jenis_pekerjaans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jenis_pekerjaans_id_seq OWNED BY public.jenis_pekerjaans.id;


--
-- Name: kategori_barangs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kategori_barangs (
    id bigint NOT NULL,
    kategori text
);


ALTER TABLE public.kategori_barangs OWNER TO postgres;

--
-- Name: kategori_barangs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kategori_barangs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kategori_barangs_id_seq OWNER TO postgres;

--
-- Name: kategori_barangs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kategori_barangs_id_seq OWNED BY public.kategori_barangs.id;


--
-- Name: master_biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.master_biodata (
    id bigint NOT NULL,
    user_id bigint,
    deskripsi_profil text,
    alamat text,
    jenis_kelamin text,
    image_ktp text,
    skill text,
    updated_at timestamp with time zone,
    created_at timestamp with time zone,
    active_work text,
    jam_kerja text,
    rate_per_days numeric,
    nik_ktp text
);


ALTER TABLE public.master_biodata OWNER TO postgres;

--
-- Name: master_biodata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.master_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.master_biodata_id_seq OWNER TO postgres;

--
-- Name: master_biodata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.master_biodata_id_seq OWNED BY public.master_biodata.id;


--
-- Name: master_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.master_users (
    id bigint NOT NULL,
    username text,
    password text,
    email text,
    phone_number text,
    role text,
    status bigint,
    token_fcm text,
    profile_picture text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    active_at timestamp with time zone,
    alasan_non_aktif text,
    saldo_koin bigint,
    date_until_active timestamp with time zone,
    badge text,
    verified bigint,
    nama_lengkap text,
    latitude double precision DEFAULT 0.0,
    longitude double precision DEFAULT 0.0
);


ALTER TABLE public.master_users OWNER TO postgres;

--
-- Name: master_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.master_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.master_users_id_seq OWNER TO postgres;

--
-- Name: master_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.master_users_id_seq OWNED BY public.master_users.id;


--
-- Name: portofolio_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.portofolio_images (
    id bigint NOT NULL,
    portofolio_id bigint,
    image text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_primary bigint DEFAULT 0,
    deskripsi text
);


ALTER TABLE public.portofolio_images OWNER TO postgres;

--
-- Name: portofolio_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.portofolio_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portofolio_images_id_seq OWNER TO postgres;

--
-- Name: portofolio_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.portofolio_images_id_seq OWNED BY public.portofolio_images.id;


--
-- Name: portofolios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.portofolios (
    id bigint NOT NULL,
    user_id bigint,
    title text,
    deskripsi text,
    status text,
    created_at timestamp with time zone,
    update_at timestamp with time zone
);


ALTER TABLE public.portofolios OWNER TO postgres;

--
-- Name: portofolios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.portofolios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portofolios_id_seq OWNER TO postgres;

--
-- Name: portofolios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.portofolios_id_seq OWNED BY public.portofolios.id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    id bigint NOT NULL,
    user_id bigint,
    judul text,
    deskripsi text,
    lokasi text,
    target_selesai text,
    id_jenis_pekerjaan bigint,
    status bigint,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    no_project text,
    estimasi_budget double precision
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO postgres;

--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;


--
-- Name: proposal_projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposal_projects (
    id bigint NOT NULL,
    no_project text,
    user_id bigint,
    is_invited text,
    status text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.proposal_projects OWNER TO postgres;

--
-- Name: proposal_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proposal_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposal_projects_id_seq OWNER TO postgres;

--
-- Name: proposal_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proposal_projects_id_seq OWNED BY public.proposal_projects.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews (
    id bigint NOT NULL,
    user_id bigint,
    rating bigint,
    no_project text,
    ulasan text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.reviews OWNER TO postgres;

--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq OWNER TO postgres;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_id_seq OWNED BY public.reviews.id;


--
-- Name: sliders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sliders (
    id bigint NOT NULL,
    image text,
    active bigint,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.sliders OWNER TO postgres;

--
-- Name: sliders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sliders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sliders_id_seq OWNER TO postgres;

--
-- Name: sliders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sliders_id_seq OWNED BY public.sliders.id;


--
-- Name: tokos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tokos (
    id bigint NOT NULL,
    user_id bigint,
    nama_toko text,
    alamat text,
    no_telp text,
    hari_operasional text,
    jam_operasional text,
    status text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.tokos OWNER TO postgres;

--
-- Name: tokos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tokos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tokos_id_seq OWNER TO postgres;

--
-- Name: tokos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tokos_id_seq OWNED BY public.tokos.id;


--
-- Name: artikels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artikels ALTER COLUMN id SET DEFAULT nextval('public.artikels_id_seq'::regclass);


--
-- Name: barangs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barangs ALTER COLUMN id SET DEFAULT nextval('public.barangs_id_seq'::regclass);


--
-- Name: jenis_pekerjaans id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_pekerjaans ALTER COLUMN id SET DEFAULT nextval('public.jenis_pekerjaans_id_seq'::regclass);


--
-- Name: kategori_barangs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kategori_barangs ALTER COLUMN id SET DEFAULT nextval('public.kategori_barangs_id_seq'::regclass);


--
-- Name: master_biodata id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_biodata ALTER COLUMN id SET DEFAULT nextval('public.master_biodata_id_seq'::regclass);


--
-- Name: master_users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_users ALTER COLUMN id SET DEFAULT nextval('public.master_users_id_seq'::regclass);


--
-- Name: portofolio_images id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portofolio_images ALTER COLUMN id SET DEFAULT nextval('public.portofolio_images_id_seq'::regclass);


--
-- Name: portofolios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portofolios ALTER COLUMN id SET DEFAULT nextval('public.portofolios_id_seq'::regclass);


--
-- Name: projects id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);


--
-- Name: proposal_projects id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposal_projects ALTER COLUMN id SET DEFAULT nextval('public.proposal_projects_id_seq'::regclass);


--
-- Name: reviews id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq'::regclass);


--
-- Name: sliders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sliders ALTER COLUMN id SET DEFAULT nextval('public.sliders_id_seq'::regclass);


--
-- Name: tokos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tokos ALTER COLUMN id SET DEFAULT nextval('public.tokos_id_seq'::regclass);


--
-- Data for Name: artikels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.artikels (id, title, image, content, views_count, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: barangs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barangs (id, id_toko, nama_barang, deskripsi_barang, foto_barang, harga, satuan, tersedia, created_at, updated_at) FROM stdin;
2	1	Semen Padang	Semen Padang cuy		60000	Sak	1	2021-12-23 03:10:49.320933+00	2021-12-23 03:10:49.320933+00
3	1	Paku 2 inchi	Paku 2 inchi		5000	Kilogram	1	2021-12-23 03:14:41.087664+00	2021-12-23 03:14:41.087664+00
\.


--
-- Data for Name: jenis_pekerjaans; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jenis_pekerjaans (id, jenis_pekerjaan) FROM stdin;
2	Mingguan
4	Borongan
1	Harian
\.


--
-- Data for Name: kategori_barangs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kategori_barangs (id, kategori) FROM stdin;
1	Semen
2	Paku
3	Lampu
\.


--
-- Data for Name: master_biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.master_biodata (id, user_id, deskripsi_profil, alamat, jenis_kelamin, image_ktp, skill, updated_at, created_at, active_work, jam_kerja, rate_per_days, nik_ktp) FROM stdin;
23	17	Hello	jambi	Laki-Laki	17-GcTyVYPW0p-Capture.PNG	relief, gali sumur, cat rumah	2021-12-21 02:25:17.262723+00	2021-12-20 07:47:54.369773+00	Senin-Jumat	08.00-17.00 WIB	130000	1571050505990041
\.


--
-- Data for Name: master_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.master_users (id, username, password, email, phone_number, role, status, token_fcm, profile_picture, created_at, updated_at, active_at, alasan_non_aktif, saldo_koin, date_until_active, badge, verified, nama_lengkap, latitude, longitude) FROM stdin;
16	oId1fKdzX6	$2a$04$bJeu78p88u6OCfS92JErrufmiHb.dqh0WZ1NKp1sZgSLZr48r9/8C	ekokurniadi@gmail.com	62895621008775	tukang	0			2021-12-17 17:41:10.08772+00	2021-12-17 17:41:10.08772+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	\N	0	0
20	BXOmdEO1RS	$2a$04$pDxgIMf4eyGIwGjM/KBjpOh4XwCNwBpOoxwjqhbKnC2q3qMx26vqO	purwanto@gmail.com	628136625617	tukang	1			2021-12-24 15:15:30.487465+00	2021-12-27 04:00:40.5177+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	purwanto	-1.5951926932489506	103.58002750300992
19	d004jpPHPW	$2a$04$NyQvApIB7nC7e.vEQUcDZe6AkprgQSqT57j.ZADcHWHLNTvOGDi.6	tokobangunan@gmail.com	6285296072648	mitra	0			2021-12-22 03:35:09.238791+00	2021-12-22 03:35:09.238791+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	Muhaimin	0	0
21	Bz9FUyNKQw	$2a$04$uy6J7BHoioa9BCG//LYzxO4a7V8MhQl9TDJJAcxbWU5ry7zLz1Svu	purwanto@gmail.com	628526625617	tukang	0			2022-01-02 06:34:55.020312+00	2022-01-02 06:34:55.020312+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	puri	0	0
13	M0oKbdkGbv	$2a$04$1n3TUQ6FfaceLkBNfok7duofO8T8/BTLYBc1v.WmydfsrstyQ3kXG	ekokurniadi@gmail.com	62895604730751	tukang	0		13-M0oKbdkGbv-iOS-1.png	2021-12-17 08:25:58.327876+00	2021-12-19 15:03:35.275545+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	Mugiono	0	0
22	z2Imb4OR4I	$2a$04$XZEd7gHmT6OoJIn24J67guiVnqTbBz5t0Lftp2Y6jp8Z12MUaV686	purwanto@gmail.com	628536625617	tukang	0			2022-01-02 06:58:18.610931+00	2022-01-02 06:58:18.610931+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	puri	0	0
23	olqpIvSBT0	$2a$04$7/RN1PoDFhk3l1a5SbV.AOiZLk1ueiBODz3f0IHYeqdAMEcOow/6W	purwanto@gmail.com	628956625617	tukang	0			2022-01-02 07:43:28.587371+00	2022-01-02 07:43:28.587371+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	puri yandi	0	0
24	PRvylwJWSS	$2a$04$mjUX5eUlT5pn/wzdfYIhiOGFaWFGtvwo1RCLM6SwukwDozHJ0jTfu	purwanto@gmail.com	628966625617	tukang	1			2022-01-02 07:46:54.969146+00	2022-01-02 07:46:54.969146+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	puri yandi	0	0
25	uTHxOvE9py	$2a$04$b4zQIRJU5BWaIYAT8RwUBOFE.05nJLtcH/i8mJ48B0L9klevU.GAG	purwanto@gmail.com	628976625617	tukang	1			2022-01-02 07:50:23.687362+00	2022-01-02 07:50:23.687362+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	puri yandi	0	0
26	92T1CVfCXm	$2a$04$zj5kqhbiRnW/Pid07koyJ.H3UUjZIBenUU7Ty7knO5XW4Twjk2PgK	purwanto@gmail.com	628986625617	tukang	1			2022-01-02 09:25:56.79558+00	2022-01-02 09:25:56.79558+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	yandi	0	0
14	wGpVUOeu8q	$2a$04$1n3TUQ6FfaceLkBNfok7duofO8T8/BTLYBc1v.WmydfsrstyQ3kXG	ekokurniadi.02@gmail.com	6285296072649	tukang	1			2021-12-17 17:36:41.339877+00	2021-12-24 15:14:32.726028+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0		0	0
15	NU7GhrmxYR	$2a$04$YUevOp5xjgf9uEyi74sZ2OMGCqWjzTI0v9XAwEK4K67X9e4RGOq4S	ekokurniadi@gmail.com	62895621008774	tukang	1			2021-12-17 17:40:29.963036+00	2021-12-17 17:40:29.963036+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	\N	0	0
17	GcTyVYPW0p	$2a$04$YhmGz.YrwaBRaEclqUnfa.muWC9dgEJ32kTyOfC/eY/b0tcNTqJua	ekokurniadi@gmail.com	628956210087	tukang	1		9578050b3d564818a9e0356043336884.png	2021-12-17 17:43:16.981291+00	2022-01-09 08:27:11.54476+00	0001-01-01 00:00:00+00		0	0001-01-01 00:00:00+00		0	Joko	0	0
\.


--
-- Data for Name: portofolio_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.portofolio_images (id, portofolio_id, image, created_at, updated_at, is_primary, deskripsi) FROM stdin;
13	39	17-GcTyVYPW0p-customer-support.jpg	2022-01-07 04:00:31.222017+00	2022-01-07 04:01:17.78012+00	0	Pemasangan Keramik
14	39	17-GcTyVYPW0p-man-renovating-his-house-with-design-space.jpg	2022-01-07 04:00:39.701628+00	2022-01-07 04:01:17.78012+00	0	Pemasangan Keramik
15	39	17-GcTyVYPW0p-workers-examining-work.jpg	2022-01-07 04:01:17.788389+00	2022-01-07 04:01:17.788389+00	1	Pemasangan Keramik
11	40	17-GcTyVYPW0p-helmet.png	2022-01-07 03:59:54.778744+00	2022-01-07 04:01:56.594327+00	0	Pemasangan Keramik
12	40	17-GcTyVYPW0p-buyer.png	2022-01-07 04:00:02.553157+00	2022-01-07 04:01:56.594327+00	0	Pemasangan Keramik
16	40	17-GcTyVYPW0p-worker-register-jumbotron.jpg	2022-01-07 04:01:56.599651+00	2022-01-07 04:01:56.599651+00	1	Pemasangan Keramik
\.


--
-- Data for Name: portofolios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.portofolios (id, user_id, title, deskripsi, status, created_at, update_at) FROM stdin;
39	17	Pembuatan kolam renang	Ini deskripsi portofolio nya	active	2022-01-07 03:55:58.195102+00	0001-01-01 00:00:00+00
40	17	Pembuatan gali sumur	Ini deskripsi portofolio nya	active	2022-01-07 03:56:12.37051+00	0001-01-01 00:00:00+00
\.


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projects (id, user_id, judul, deskripsi, lokasi, target_selesai, id_jenis_pekerjaan, status, created_at, updated_at, no_project, estimasi_budget) FROM stdin;
\.


--
-- Data for Name: proposal_projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proposal_projects (id, no_project, user_id, is_invited, status, created_at, updated_at) FROM stdin;
33	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:23:15.150746+00	2021-12-24 15:23:15.150746+00
34	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:28:39.72256+00	2021-12-24 15:28:39.72256+00
35	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:36:17.157651+00	2021-12-24 15:36:17.157651+00
36	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:38:37.787505+00	2021-12-24 15:38:37.787505+00
37	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:39:10.065122+00	2021-12-24 15:39:10.065122+00
38	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:39:38.813379+00	2021-12-24 15:39:38.813379+00
39	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:44:21.353271+00	2021-12-24 15:44:21.353271+00
40	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:46:56.211829+00	2021-12-24 15:46:56.211829+00
41	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:48:45.74321+00	2021-12-24 15:48:45.74321+00
42	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:50:57.165478+00	2021-12-24 15:50:57.165478+00
43	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 15:55:45.734857+00	2021-12-24 15:55:45.734857+00
44	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 16:00:16.065026+00	2021-12-24 16:00:16.065026+00
45	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 16:00:22.576457+00	2021-12-24 16:00:22.576457+00
46	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 16:00:24.359133+00	2021-12-24 16:00:24.359133+00
47	PRJ-GcTyVYPW0p-00001	20	No	pengajuan	2021-12-24 16:02:14.035304+00	2021-12-24 16:02:14.035304+00
48	PRJ-GcTyVYPW0p-00001	20	No	submitted	2021-12-24 17:23:17.519264+00	2021-12-24 17:23:17.519264+00
49	PRJ-GcTyVYPW0p-00001	0	Yes	invited	2021-12-24 17:26:42.606685+00	2021-12-24 17:26:42.606685+00
50	PRJ-GcTyVYPW0p-00001	13	Yes	invited	2021-12-24 17:30:26.205437+00	2021-12-24 17:30:26.205437+00
51	PRJ-GcTyVYPW0p-00001	13	Yes	invited	2021-12-24 17:39:37.259789+00	2021-12-24 17:39:37.259789+00
52	PRJ-GcTyVYPW0p-00001	13	Yes	invited	2021-12-24 17:40:59.406532+00	2021-12-24 17:40:59.406532+00
53	PRJ-GcTyVYPW0p-00001	13	Yes	invited	2021-12-24 17:42:46.892945+00	2021-12-24 17:42:46.892945+00
54	PRJ-GcTyVYPW0p-00001	15	Yes	invited	2021-12-24 17:46:38.84595+00	2021-12-24 17:46:38.84595+00
55	PRJ-GcTyVYPW0p-00001	16	Yes	invited	2021-12-24 17:47:07.383797+00	2021-12-24 17:47:07.383797+00
\.


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews (id, user_id, rating, no_project, ulasan, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: sliders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sliders (id, image, active, created_at, updated_at) FROM stdin;
58	db91d10c26be47b8993be720a14af743.jpg	1	2022-01-07 08:57:32.266394+00	2022-01-07 08:57:32.266394+00
59	3fe582719a9a43d6a0714d9e2656400e.jpg	1	2022-01-07 08:57:46.165772+00	2022-01-07 08:57:46.165772+00
60	707ef2101c6148b7b4535b31d5473d32.jpg	1	2022-01-07 08:58:00.795034+00	2022-01-07 09:14:35.696693+00
61	b61bb6d58a9744a8af6cbaf7afdac550.jpg	1	2022-01-09 06:46:42.716638+00	2022-01-09 06:46:42.716638+00
62	51bcfe80338d4f7e82b21dbd5cafabde.jpg	1	2022-01-09 07:09:12.623759+00	2022-01-09 07:09:12.623759+00
63	7bc663da342b447f9ed6ac1dc994ac77.jpg	1	2022-01-09 07:09:31.145548+00	2022-01-09 07:09:31.145548+00
64	9edf6e3b06f14b95b10a47a12682fffc.png	1	2022-01-09 07:09:40.565191+00	2022-01-09 07:09:40.565191+00
65	2396fc6fbb5744cebbba7dd09c502fb7.jpg	1	2022-01-09 07:09:54.879576+00	2022-01-09 07:09:54.879576+00
66	ab2d32c175544e0485e0e1dfd745adaa.jpg	1	2022-01-09 07:10:16.030641+00	2022-01-09 07:10:16.030641+00
67	ddeb7dc6472849e291ae0a2f49da8f6d.png	1	2022-01-09 07:10:27.636478+00	2022-01-09 07:10:27.636478+00
69	5240b1a164be40f09b0ae1b4b1e51120.jpg	1	2022-01-09 07:18:54.393607+00	2022-01-09 07:18:54.393607+00
70	db98299875ae4790b1030d0ac6c446c6.jpg	1	2022-01-09 07:19:44.930133+00	2022-01-09 07:19:44.930133+00
\.


--
-- Data for Name: tokos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tokos (id, user_id, nama_toko, alamat, no_telp, hari_operasional, jam_operasional, status, created_at, updated_at) FROM stdin;
1	19	Toko Bangunan Indah Berseri	Jln. Kopral Ramli, No.25	074125256	Senin-Minggu	08.00-16.00 WIB	open	2021-12-22 03:42:23.688709+00	2021-12-22 03:42:23.688709+00
\.


--
-- Name: artikels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.artikels_id_seq', 1, false);


--
-- Name: barangs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barangs_id_seq', 3, true);


--
-- Name: jenis_pekerjaans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jenis_pekerjaans_id_seq', 4, true);


--
-- Name: kategori_barangs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kategori_barangs_id_seq', 4, true);


--
-- Name: master_biodata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.master_biodata_id_seq', 23, true);


--
-- Name: master_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.master_users_id_seq', 26, true);


--
-- Name: portofolio_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.portofolio_images_id_seq', 16, true);


--
-- Name: portofolios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.portofolios_id_seq', 40, true);


--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projects_id_seq', 28, true);


--
-- Name: proposal_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proposal_projects_id_seq', 55, true);


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_id_seq', 1, false);


--
-- Name: sliders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sliders_id_seq', 70, true);


--
-- Name: tokos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tokos_id_seq', 1, true);


--
-- Name: artikels artikels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artikels
    ADD CONSTRAINT artikels_pkey PRIMARY KEY (id);


--
-- Name: barangs barangs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barangs
    ADD CONSTRAINT barangs_pkey PRIMARY KEY (id);


--
-- Name: jenis_pekerjaans jenis_pekerjaans_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_pekerjaans
    ADD CONSTRAINT jenis_pekerjaans_pkey PRIMARY KEY (id);


--
-- Name: kategori_barangs kategori_barangs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kategori_barangs
    ADD CONSTRAINT kategori_barangs_pkey PRIMARY KEY (id);


--
-- Name: master_biodata master_biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_biodata
    ADD CONSTRAINT master_biodata_pkey PRIMARY KEY (id);


--
-- Name: master_users master_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_users
    ADD CONSTRAINT master_users_pkey PRIMARY KEY (id);


--
-- Name: portofolio_images portofolio_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portofolio_images
    ADD CONSTRAINT portofolio_images_pkey PRIMARY KEY (id);


--
-- Name: portofolios portofolios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portofolios
    ADD CONSTRAINT portofolios_pkey PRIMARY KEY (id);


--
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: proposal_projects proposal_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposal_projects
    ADD CONSTRAINT proposal_projects_pkey PRIMARY KEY (id);


--
-- Name: reviews reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: sliders sliders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sliders
    ADD CONSTRAINT sliders_pkey PRIMARY KEY (id);


--
-- Name: tokos tokos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tokos
    ADD CONSTRAINT tokos_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

