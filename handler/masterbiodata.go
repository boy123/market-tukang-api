package handler

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type masterbiodataHandler struct {
	service service.MasterBiodataService
}

func NewMasterBiodataHandler(service service.MasterBiodataService) *masterbiodataHandler {
	return &masterbiodataHandler{service}
}
func (h *masterbiodataHandler) GetMasterBiodata(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	masterbiodataDetail, err := h.service.MasterBiodataServiceGetByUserID(userID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterBiodata", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail MasterBiodata", http.StatusOK, "success", formatter.FormatMasterBiodata(masterbiodataDetail))
	c.JSON(http.StatusOK, response)
}

func (h *masterbiodataHandler) GetMasterBiodatas(c *gin.Context) {
	masterbiodatas, err := h.service.MasterBiodataServiceGetAll()
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterBiodatas", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of MasterBiodatas", http.StatusOK, "success", formatter.FormatMasterBiodatas(masterbiodatas))
	c.JSON(http.StatusOK, response)
}

func (h *masterbiodataHandler) CreateMasterBiodata(c *gin.Context) {
	var inputData input.MasterBiodataInput
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	inputData.UserID = userID
	inputData.User.NamaLengkap = currentUser.NamaLengkap
	err := c.ShouldBind(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Gagal menyimpan data biodata", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	path := fmt.Sprintf("images/ktp/%d-%s-%s", userID, currentUser.Username, inputData.ImageKTP.Filename)

	if inputData.ImageKTP.Filename != "" {
		err = c.SaveUploadedFile(inputData.ImageKTP, path)

		if err != nil {
			pc, fn, line, _ := runtime.Caller(0)
			errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
			helper.PrintErr(err, errornya)

			data := gin.H{
				"is_uploaded": false,
			}
			response := helper.ApiResponse("Gagal mengupload foto KTP", http.StatusOK, "error", data)
			c.AbortWithStatusJSON(http.StatusOK, response)
			return
		}
	}

	existingUser, _ := h.service.MasterBiodataServiceGetByUserID(userID)
	filepath, _ := filepath.Abs("./images/ktp" + "/" + existingUser.ImageKTP)

	if filepath != "" {
		os.Remove(filepath)
	}
	if existingUser.UserID == 0 {
		inputData.User.Username = currentUser.Username
		newMasterBiodata, err := h.service.MasterBiodataServiceCreate(inputData)
		if err != nil {
			pc, fn, line, _ := runtime.Caller(0)
			errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
			helper.PrintErr(err, errornya)

			response := helper.ApiResponse("Biodata berhasil di perbarui", http.StatusOK, "error", nil)
			c.JSON(http.StatusOK, response)
			return
		}
		response := helper.ApiResponse("Biodata berhasil di perbarui", http.StatusOK, "success", formatter.FormatMasterBiodata(newMasterBiodata))
		c.JSON(http.StatusOK, response)
	} else {
		var inputID input.InputIDMasterBiodata
		inputID.ID = existingUser.ID
		inputData.User.Username = currentUser.Username
		updatedUser, err := h.service.MasterBiodataServiceUpdate(inputID, inputData)
		if err != nil {
			pc, fn, line, _ := runtime.Caller(0)
			errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
			helper.PrintErr(err, errornya)

			response := helper.ApiResponse("Biodata berhasil di perbarui", http.StatusOK, "error", nil)
			c.JSON(http.StatusOK, response)
			return
		}
		response := helper.ApiResponse("Biodata berhasil di perbarui", http.StatusOK, "success", formatter.FormatMasterBiodata(updatedUser))
		c.JSON(http.StatusOK, response)
	}

}
func (h *masterbiodataHandler) UpdateMasterBiodata(c *gin.Context) {
	var inputID input.InputIDMasterBiodata
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterBiodatas", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.MasterBiodataInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update MasterBiodata failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	updatedMasterBiodata, err := h.service.MasterBiodataServiceUpdate(inputID, inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterBiodatas", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update MasterBiodata", http.StatusOK, "success", formatter.FormatMasterBiodata(updatedMasterBiodata))
	c.JSON(http.StatusOK, response)
}
func (h *masterbiodataHandler) DeleteMasterBiodata(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDMasterBiodata
	inputID.ID = id
	_, err := h.service.MasterBiodataServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterBiodatas", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.MasterBiodataServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterBiodatas", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete MasterBiodata", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
