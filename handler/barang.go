package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type barangHandler struct {
	service     service.BarangService
	tokoService service.TokoService
}

func NewBarangHandler(service service.BarangService, tokoService service.TokoService) *barangHandler {
	return &barangHandler{service, tokoService}
}
func (h *barangHandler) GetBarang(c *gin.Context) {
	var input input.InputIDBarang
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barang", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	barangDetail, err := h.service.BarangServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barang", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	response := helper.ApiResponse("Detail Barang", http.StatusOK, "success", formatter.FormatBarang(barangDetail))
	c.JSON(http.StatusOK, response)
}

func (h *barangHandler) GetBarangs(c *gin.Context) {
	barangs, err := h.service.BarangServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Barangs", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	response := helper.ApiResponse("List of Barangs", http.StatusOK, "success", formatter.FormatBarangs(barangs))
	c.JSON(http.StatusOK, response)
}

func (h *barangHandler) GetAllBarangByUserID(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	barangs, err := h.service.BarangServiceGetByUserID(userID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barangs", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	response := helper.ApiResponse("List of Barangs", http.StatusOK, "success", formatter.FormatBarang(barangs))
	c.JSON(http.StatusOK, response)
}

func (h *barangHandler) CreateBarang(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	toko, err := h.tokoService.TokoServiceGetByUserID(userID)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Barang failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}

	var input input.BarangInput
	err = c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Barang failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	input.IDToko = toko.ID
	newBarang, err := h.service.BarangServiceCreate(input)
	if err != nil {
		response := helper.ApiResponse("Create Barang failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	response := helper.ApiResponse("Successfully Create Barang", http.StatusOK, "success", formatter.FormatBarang(newBarang))
	c.JSON(http.StatusOK, response)
}
func (h *barangHandler) UpdateBarang(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	toko, err := h.tokoService.TokoServiceGetByUserID(userID)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Barang failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	var inputID input.InputIDBarang
	err = c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barangs", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	var inputData input.BarangInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Barang failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	inputData.IDToko = toko.ID

	if toko.UserID != currentUser.ID {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Barang failed", http.StatusUnauthorized, "error", errorMessage)
		c.JSON(http.StatusUnauthorized, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}

	updatedBarang, err := h.service.BarangServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barangs", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	response := helper.ApiResponse("Successfully Update Barang", http.StatusOK, "success", formatter.FormatBarang(updatedBarang))
	c.JSON(http.StatusOK, response)
}
func (h *barangHandler) DeleteBarang(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDBarang
	inputID.ID = id
	_, err := h.service.BarangServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barangs", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	toko, err := h.tokoService.TokoServiceGetByUserID(userID)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Delete Barang failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}

	if toko.UserID != currentUser.ID {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Delete Barang failed", http.StatusUnauthorized, "error", errorMessage)
		c.JSON(http.StatusUnauthorized, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}

	_, err = h.service.BarangServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Barangs", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))
		helper.Logger.Println(err)
		return
	}
	response := helper.ApiResponse("Successfully Delete Barang", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
