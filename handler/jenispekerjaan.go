package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type jenispekerjaanHandler struct {
	service service.JenisPekerjaanService
}

func NewJenisPekerjaanHandler(service service.JenisPekerjaanService) *jenispekerjaanHandler {
	return &jenispekerjaanHandler{service}
}

func (h *jenispekerjaanHandler) GetJenisPekerjaan(c *gin.Context) {
	var input input.InputIDJenisPekerjaan
	err := c.ShouldBindUri(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaan", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	jenispekerjaanDetail, err := h.service.JenisPekerjaanServiceGetByID(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaan", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail JenisPekerjaan", http.StatusOK, "success", formatter.FormatJenisPekerjaan(jenispekerjaanDetail))
	c.JSON(http.StatusOK, response)
}

func (h *jenispekerjaanHandler) GetJenisPekerjaans(c *gin.Context) {
	jenispekerjaans, err := h.service.JenisPekerjaanServiceGetAll()
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaans", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of JenisPekerjaans", http.StatusOK, "success", formatter.FormatJenisPekerjaans(jenispekerjaans))
	c.JSON(http.StatusOK, response)
}

func (h *jenispekerjaanHandler) CreateJenisPekerjaan(c *gin.Context) {
	var input input.JenisPekerjaanInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create JenisPekerjaan failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	newJenisPekerjaan, err := h.service.JenisPekerjaanServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Create JenisPekerjaan failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Create JenisPekerjaan", http.StatusOK, "success", formatter.FormatJenisPekerjaan(newJenisPekerjaan))
	c.JSON(http.StatusOK, response)
}
func (h *jenispekerjaanHandler) UpdateJenisPekerjaan(c *gin.Context) {
	var inputID input.InputIDJenisPekerjaan
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaans", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.JenisPekerjaanInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update JenisPekerjaan failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	updatedJenisPekerjaan, err := h.service.JenisPekerjaanServiceUpdate(inputID, inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaans", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update JenisPekerjaan", http.StatusOK, "success", formatter.FormatJenisPekerjaan(updatedJenisPekerjaan))
	c.JSON(http.StatusOK, response)
}
func (h *jenispekerjaanHandler) DeleteJenisPekerjaan(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDJenisPekerjaan
	inputID.ID = id
	_, err := h.service.JenisPekerjaanServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaans", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.JenisPekerjaanServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get JenisPekerjaans", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete JenisPekerjaan", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
