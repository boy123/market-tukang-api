package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type projectHandler struct {
	service         service.ProjectService
	proposalService service.ProposalProjectService
}

func NewProjectHandler(service service.ProjectService, proposalService service.ProposalProjectService) *projectHandler {
	return &projectHandler{service, proposalService}
}
func (h *projectHandler) GetProject(c *gin.Context) {
	var input input.InputIDProject
	err := c.ShouldBindUri(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Project", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	projectDetail, err := h.service.ProjectServiceGetByID(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Project", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail Project", http.StatusOK, "success", formatter.FormatProject(projectDetail))
	c.JSON(http.StatusOK, response)
}

func (h *projectHandler) GetProjects(c *gin.Context) {
	projects, err := h.service.ProjectServiceGetAll()
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of Projects", http.StatusOK, "success", formatter.FormatProjects(projects))
	c.JSON(http.StatusOK, response)
}

func (h *projectHandler) GetProjectsByUserID(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	projects, err := h.service.ProjectServiceGetAllByUser(userID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of Projects", http.StatusOK, "success", formatter.FormatProjects(projects))
	c.JSON(http.StatusOK, response)
}

func (h *projectHandler) CreateProject(c *gin.Context) {
	var input input.ProjectInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Project failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	input.UserIDClient = userID
	input.User.Username = currentUser.Username
	newProject, err := h.service.ProjectServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Create Project failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Project", http.StatusOK, "success", formatter.FormatProject(newProject))
	c.JSON(http.StatusOK, response)
}
func (h *projectHandler) UpdateProject(c *gin.Context) {
	var inputID input.InputIDProject
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.ProjectInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Project failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	updatedProject, err := h.service.ProjectServiceUpdate(inputID, inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Project", http.StatusOK, "success", formatter.FormatProject(updatedProject))
	c.JSON(http.StatusOK, response)
}
func (h *projectHandler) DeleteProject(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDProject
	inputID.ID = id
	_, err := h.service.ProjectServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.ProjectServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Project", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}

func (h *projectHandler) InviteTukang(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDProject
	inputID.ID = id
	project, err := h.service.ProjectServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var input input.ProposalProjectInput
	err = c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Projects", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	input.NoProject = project.NoProject
	input.IsInvited = "Yes"
	input.Status = "invited"

	proposal, err := h.proposalService.ProposalProjectServiceGetByNoProject(project.NoProject, input.UserID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal mengundang tukang", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	if proposal.NoProject == project.NoProject && proposal.UserID == input.UserID {
		response := helper.ApiResponse("Tukang sudah pernah diundang pada project ini", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)

		return
	}

	_, err = h.proposalService.ProposalProjectServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal mengundang tukang", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)

		return
	}
	response := helper.ApiResponse("Undangan berhasil dikirim ke tukang", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)

}
