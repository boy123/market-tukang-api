package handler

import (
	"fmt"
	"math"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/repository"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type artikelHandler struct {
	service    service.ArtikelService
	repository repository.ArtikelRepository
}

func NewArtikelHandler(service service.ArtikelService, repository repository.ArtikelRepository) *artikelHandler {
	return &artikelHandler{service, repository}
}
func (h *artikelHandler) GetArtikel(c *gin.Context) {
	var input input.InputIDArtikel
	err := c.ShouldBindUri(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Artikel", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	artikelDetail, err := h.service.ArtikelServiceGetByID(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Artikel", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail Artikel", http.StatusOK, "success", formatter.FormatArtikel(artikelDetail))
	c.JSON(http.StatusOK, response)
}

func (h *artikelHandler) GetArtikels(c *gin.Context) {
	query := c.Query("query")
	page := c.Query("page")
	size := c.Query("size")
	convertedPage, _ := strconv.Atoi(page)
	convertedSize, _ := strconv.Atoi(size)
	artikels, err := h.service.ArtikelServiceGetAll(query, convertedPage, convertedSize)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Artikels", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of Artikels", http.StatusOK, "success", formatter.NewFormatArtikels(artikels))
	c.JSON(http.StatusOK, response)
}

func (h *artikelHandler) CreateArtikel(c *gin.Context) {
	var input input.ArtikelInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Artikel failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	newArtikel, err := h.service.ArtikelServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Create Artikel failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Artikel", http.StatusOK, "success", formatter.FormatArtikel(newArtikel))
	c.JSON(http.StatusOK, response)
}
func (h *artikelHandler) UpdateArtikel(c *gin.Context) {
	var inputID input.InputIDArtikel
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Artikels", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.ArtikelInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to update Artikels", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	updatedArtikel, err := h.service.ArtikelServiceUpdate(inputID, inputData)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		response := helper.ApiResponse("Failed to get Artikel", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Artikel", http.StatusOK, "success", formatter.FormatArtikel(updatedArtikel))
	c.JSON(http.StatusOK, response)

}
func (h *artikelHandler) DeleteArtikel(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDArtikel
	inputID.ID = id

	_, err := h.service.ArtikelServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Artikels", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Artikel", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}

func (h *artikelHandler) FetchData(c *gin.Context) {
	search := c.Query("title")
	page := c.Query("page")
	size := c.Query("size")

	convertPage, _ := strconv.Atoi(page)
	convertSize, _ := strconv.Atoi(size)

	fetchingData, err := h.service.ArtikelServiceFetchData(search, convertPage, convertSize)
	if err != nil {
		response := helper.ApiResponse("Failed to get data", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	recordsFiltered, err := h.repository.GetTotalFetchData(search, convertPage, convertSize)
	if err != nil {
		response := helper.ApiResponse("Failed to get data", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	totalPages := math.Ceil(float64(recordsFiltered) / float64(convertSize))
	currentPage := convertPage + 1

	response := helper.WebApiResponse(recordsFiltered, int(totalPages), currentPage, formatter.FormatArtikels(fetchingData))
	c.JSON(http.StatusOK, response)
}
