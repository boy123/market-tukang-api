package handler

import (
	"fmt"
	"math"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/repository"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type sliderHandler struct {
	service    service.SliderService
	repository repository.SliderRepository
}

func NewSliderHandler(service service.SliderService, repository repository.SliderRepository) *sliderHandler {
	return &sliderHandler{service, repository}
}
func (h *sliderHandler) GetSlider(c *gin.Context) {
	var input input.InputIDSlider
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slider", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	sliderDetail, err := h.service.SliderServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slider", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail Slider", http.StatusOK, "success", formatter.FormatSlider(sliderDetail))
	c.JSON(http.StatusOK, response)
}

func (h *sliderHandler) GetSliders(c *gin.Context) {
	sliders, err := h.service.SliderServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Sliders", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of Sliders", http.StatusOK, "success", formatter.FormatSliders(sliders))
	c.JSON(http.StatusOK, response)
}

func (h *sliderHandler) CreateSlider(c *gin.Context) {
	var input input.SliderInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Create", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	newSlider, err := h.service.SliderServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Create Slider failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Slider", http.StatusOK, "success", formatter.FormatSlider(newSlider))
	c.JSON(http.StatusOK, response)
}
func (h *sliderHandler) UpdateSlider(c *gin.Context) {
	var inputID input.InputIDSlider
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Sliders", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.SliderInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Sliders", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	updatedSlider, err := h.service.SliderServiceUpdate(inputID, inputData)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		response := helper.ApiResponse("Failed to get Sliders", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Slider", http.StatusOK, "success", formatter.FormatSlider(updatedSlider))
	c.JSON(http.StatusOK, response)

}
func (h *sliderHandler) DeleteSlider(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDSlider
	inputID.ID = id
	_, err := h.service.SliderServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Sliders", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.SliderServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Sliders", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Slider", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}

func (h *sliderHandler) FetchData(c *gin.Context) {
	search := c.Query("title")
	page := c.Query("page")
	size := c.Query("size")

	convertPage, _ := strconv.Atoi(page)
	convertSize, _ := strconv.Atoi(size)

	fetchingData, err := h.service.SliderServiceFetchData(search, convertPage, convertSize)
	if err != nil {
		response := helper.ApiResponse("Failed to get data", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	recordsFiltered, err := h.repository.GetTotalFetchData(search, convertPage, convertSize)
	if err != nil {
		response := helper.ApiResponse("Failed to get data", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	totalPages := math.Ceil(float64(recordsFiltered) / float64(convertSize))
	currentPage := convertPage + 1

	response := helper.WebApiResponse(recordsFiltered, int(totalPages), currentPage, formatter.FormatWebDatas(fetchingData))
	c.JSON(http.StatusOK, response)

}
