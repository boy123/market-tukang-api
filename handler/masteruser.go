package handler

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/middleware"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type masteruserHandler struct {
	service     service.MasterUserService
	authService middleware.Service
}

func NewMasterUserHandler(service service.MasterUserService, authService middleware.Service) *masteruserHandler {
	return &masteruserHandler{service, authService}
}
func (h *masteruserHandler) GetMasterUser(c *gin.Context) {
	var input input.InputIDMasterUser
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	input.ID = userID
	masteruserDetail, err := h.service.MasterUserServiceGetByID(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal mendapatkan data  profil user", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail profil user", http.StatusOK, "success", formatter.FormatMasterUser(masteruserDetail))
	c.JSON(http.StatusOK, response)
}

func (h *masteruserHandler) FetchUser(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)

	formatter := formatter.FormatUser(currentUser, "")
	response := helper.ApiResponse("Successfuly fetch user data", http.StatusOK, "success", formatter)
	c.JSON(http.StatusOK, response)

}

func (h *masteruserHandler) Option(c *gin.Context) {
	c.JSON(http.StatusOK, "OK")
}

func (h *masteruserHandler) GetMasterUsers(c *gin.Context) {
	masterusers, err := h.service.MasterUserServiceGetAll()
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterUsers", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of MasterUsers", http.StatusOK, "success", formatter.FormatMasterUsers(masterusers))
	c.JSON(http.StatusOK, response)
}

func (h *masteruserHandler) CreateMasterUser(c *gin.Context) {
	var input input.MasterUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create MasterUser failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	newMasterUser, err := h.service.MasterUserServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Create MasterUser failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Create MasterUser", http.StatusOK, "success", formatter.FormatMasterUser(newMasterUser))
	c.JSON(http.StatusOK, response)
}
func (h *masteruserHandler) UpdateMasterUser(c *gin.Context) {
	var inputID input.InputIDMasterUser
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterUsers", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.MasterUserInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update MasterUser failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	updatedMasterUser, err := h.service.MasterUserServiceUpdate(inputID, inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterUsers", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update MasterUser", http.StatusOK, "success", formatter.FormatMasterUser(updatedMasterUser))
	c.JSON(http.StatusOK, response)
}
func (h *masteruserHandler) DeleteMasterUser(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDMasterUser
	inputID.ID = id
	_, err := h.service.MasterUserServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterUsers", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.MasterUserServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get MasterUsers", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete MasterUser", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}

func (h *masteruserHandler) Register(c *gin.Context) {
	var inputData input.MasterUserInput
	err := c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Registrasi akun gagal, silahkan coba kembali", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	isExist, err := h.service.MasterUserServiceGetByPhoneNumber(inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Registrasi akun gagal, silahkan coba kembali", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	if isExist.ID != 0 {
		response := helper.ApiResponse("Nomor anda sudah terdaftar, silahkan login ke akun anda", http.StatusOK, "success", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	newUser, err := h.service.MasterUserServiceCreate(inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Registrasi akun gagal, silahkan coba kembali", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	token, err := h.authService.GenerateToken(newUser.ID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Registrasi akun gagal, silahkan coba kembali", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	inputElastic := input.MasterUserInputElastic{}
	inputElastic.ID = newUser.ID
	inputElastic.NamaLengkap = newUser.NamaLengkap
	inputElastic.Username = newUser.Username
	inputElastic.Role = newUser.Role
	inputElastic.PhoneNumber = newUser.PhoneNumber
	inputElastic.Email = newUser.Email

	// _, err = h.service.MasterUserServiceInsertToIndexUser(newUser.ID, inputElastic)
	// if err != nil {
	// 	helper.PrintErr(err, runtime.FuncForPC(pc).Name(), fn, line)
	//
	// 	fmt.Println(err.Error())
	// }

	formatter := formatter.FormatUser(newUser, token)
	response := helper.ApiResponse("Registrasi akun berhasil", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

func (h *masteruserHandler) Login(c *gin.Context) {
	var inputs input.MasterUserInput
	err := c.ShouldBindJSON(&inputs)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response :=
			helper.ApiResponse("Login Account failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	loggedinUser, err := h.service.MasterUserServiceLogin(inputs)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errorMessage := gin.H{"errors": err.Error()}
		response := helper.ApiResponse(err.Error(), http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	token, err := h.authService.GenerateToken(loggedinUser.ID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Login gagal, silahkan coba kembali", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	updatedTokenFcm, err := h.service.MasterUserServiceUpdateToken(loggedinUser.ID, inputs)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal update token, silahkan coba kembali", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	if loggedinUser.Status == 0 {
		response := helper.ApiResponse("Login gagal, Akun anda sedang tidak aktif, silahkan hubungi admin", http.StatusUnauthorized, "error", nil)
		c.JSON(http.StatusUnauthorized, response)
		return
	}

	formatter := formatter.FormatUser(updatedTokenFcm, token)
	response :=
		helper.ApiResponse("Login success", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}
func (h *masteruserHandler) LoginAdmin(c *gin.Context) {
	var inputs input.MasterUserInput
	err := c.ShouldBindJSON(&inputs)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response :=
			helper.ApiResponse("Login Account failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	loggedinUser, err := h.service.MasterUserServiceLoginAdmin(inputs)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errorMessage := gin.H{"errors": err.Error()}
		response := helper.ApiResponse(err.Error(), http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	token, err := h.authService.GenerateToken(loggedinUser.ID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Login gagal, silahkan coba kembali", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	updatedTokenFcm, err := h.service.MasterUserServiceUpdateToken(loggedinUser.ID, inputs)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal update token, silahkan coba kembali", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	if loggedinUser.Status == 0 {
		response := helper.ApiResponse("Login gagal, Akun anda sedang tidak aktif, silahkan hubungi admin", http.StatusUnauthorized, "error", nil)
		c.JSON(http.StatusUnauthorized, response)
		return
	}

	formatter := formatter.FormatUser(updatedTokenFcm, token)
	response :=
		helper.ApiResponse("Login success", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

func (h *masteruserHandler) ReNewToken(c *gin.Context) {
	var input input.MasterUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response :=
			helper.ApiResponse("Gagal memperbarui token", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	loggedinUser, err := h.service.MasterUserServiceLogin(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errorMessage := gin.H{"errors": err.Error()}
		response := helper.ApiResponse("Gagal memperbarui token, silahkan coba kembali", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	token, err := h.authService.GenerateToken(loggedinUser.ID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal memperbarui token, silahkan coba kembali", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	formatter := formatter.FormatUser(loggedinUser, token)
	response := helper.ApiResponse("Berhasil memperbarui token", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

func (h *masteruserHandler) UploadPhotoProfile(c *gin.Context) {
	file, err := c.FormFile("profile_picture")
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		data := gin.H{
			"is_uploaded": false,
		}
		response := helper.ApiResponse("Gagal mengupload foto profile", http.StatusOK, "error", data)
		c.JSON(http.StatusOK, response)
		return
	}

	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	path := fmt.Sprintf("images/profile_picture/%d-%s-%s", userID, currentUser.Username, file.Filename)

	fileName := fmt.Sprintf("%d-%s-%s", userID, currentUser.Username, file.Filename)

	filepath, _ := filepath.Abs("./images/profile_picture" + "/" + currentUser.ProfilePicture)
	if filepath != "" {
		os.Remove(filepath)
	}
	err = c.SaveUploadedFile(file, path)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		data := gin.H{
			"is_uploaded": false,
		}
		response := helper.ApiResponse("Gagal mengupload foto profile", http.StatusOK, "error", data)
		c.JSON(http.StatusOK, response)
		return
	}

	updatedUser, err := h.service.MasterUserServiceUploadProfilePicture(userID, fileName)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		data := gin.H{
			"is_uploaded": false,
		}
		response := helper.ApiResponse("Failed to upload avatar image", http.StatusOK, "error", data)
		c.JSON(http.StatusOK, response)
		return
	}
	data := gin.H{
		"is_uploaded":     true,
		"profile_picture": "/profile_picture/" + updatedUser.ProfilePicture,
	}

	response := helper.ApiResponse("Successfully Uploaded", http.StatusOK, "success", data)
	c.JSON(http.StatusOK, response)

}

func (h *masteruserHandler) UpdateLokasi(c *gin.Context) {
	var input input.UserLocationInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Terjadi kesalahan format data", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	updatedUser, err := h.service.MasterUserServiceUpdateLocation(userID, input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Gagal mengupdate lokasi user", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	formatter := formatter.FormatMasterUser(updatedUser)
	response := helper.ApiResponse("Berhasil mengupdate lokasi", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

// func (h *masteruserHandler) SearchUserOnElastic(c *gin.Context) {
// 	q := c.Query("q")
// 	newUser, err := h.service.SearchUser(q)
// 	if err != nil {
// 		helper.PrintErr(err, runtime.FuncForPC(pc).Name(), fn, line)
//
// 		response := helper.ApiResponse("Terjadi kesalahan silahkan coba lagi", http.StatusOK, "error", nil)
// 		c.JSON(http.StatusOK, response)
// 		return
// 	}

// 	formatter := formatter.FormatMasterUsers(newUser)
// 	response := helper.ApiResponse("List Of Users", http.StatusOK, "success", formatter)
// 	c.JSON(http.StatusOK, response)
// }
