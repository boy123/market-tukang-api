package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type errorLogHandler struct {
	service service.ErrorLogService
}

func NewErrorLogHandler(service service.ErrorLogService) *errorLogHandler {
	return &errorLogHandler{service}
}

func (h *errorLogHandler) GetAll(c *gin.Context) {
	searchQuery := c.Query("query")
	page := c.Query("page")
	size := c.Query("size")

	convertSize, _ := strconv.Atoi(page)
	convertPage, _ := strconv.Atoi(size)

	fetchData, err := h.service.GetAll(searchQuery, convertPage, convertSize)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
	}

	c.JSON(http.StatusOK, fetchData)
}
