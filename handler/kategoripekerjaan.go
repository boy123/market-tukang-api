package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type kategoripekerjaanHandler struct {
	service service.KategoriPekerjaanService
}

func NewKategoriPekerjaanHandler(service service.KategoriPekerjaanService) *kategoripekerjaanHandler {
	return &kategoripekerjaanHandler{service}
}
func (h *kategoripekerjaanHandler) GetKategoriPekerjaan(c *gin.Context) {
	var input input.InputIDKategoriPekerjaan
	err := c.ShouldBindUri(&input)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaan", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	kategoripekerjaanDetail, err := h.service.KategoriPekerjaanServiceGetByID(input)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaan", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail KategoriPekerjaan", http.StatusOK, "success", formatter.FormatKategoriPekerjaan(kategoripekerjaanDetail))
	c.JSON(http.StatusOK, response)
}

func (h *kategoripekerjaanHandler) GetKategoriPekerjaans(c *gin.Context) {
	kategoripekerjaans, err := h.service.KategoriPekerjaanServiceGetAll()
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of KategoriPekerjaans", http.StatusOK, "success", formatter.FormatKategoriPekerjaans(kategoripekerjaans))
	c.JSON(http.StatusOK, response)
}

func (h *kategoripekerjaanHandler) CreateKategoriPekerjaan(c *gin.Context) {
	var input input.KategoriPekerjaanInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create KategoriPekerjaan failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	newKategoriPekerjaan, err := h.service.KategoriPekerjaanServiceCreate(input)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Create KategoriPekerjaan failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create KategoriPekerjaan", http.StatusOK, "success", formatter.FormatKategoriPekerjaan(newKategoriPekerjaan))
	c.JSON(http.StatusOK, response)
}
func (h *kategoripekerjaanHandler) UpdateKategoriPekerjaan(c *gin.Context) {
	var inputID input.InputIDKategoriPekerjaan
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.KategoriPekerjaanInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update KategoriPekerjaan failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedKategoriPekerjaan, err := h.service.KategoriPekerjaanServiceUpdate(inputID, inputData)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update KategoriPekerjaan", http.StatusOK, "success", formatter.FormatKategoriPekerjaan(updatedKategoriPekerjaan))
	c.JSON(http.StatusOK, response)
}
func (h *kategoripekerjaanHandler) DeleteKategoriPekerjaan(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDKategoriPekerjaan
	inputID.ID = id
	_, err := h.service.KategoriPekerjaanServiceGetByID(inputID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.KategoriPekerjaanServiceDeleteByID(inputID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		response := helper.ApiResponse("Failed to get KategoriPekerjaans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete KategoriPekerjaan", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
