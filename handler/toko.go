package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type tokoHandler struct {
	service service.TokoService
}

func NewTokoHandler(service service.TokoService) *tokoHandler {
	return &tokoHandler{service}
}
func (h *tokoHandler) GetToko(c *gin.Context) {
	var input input.InputIDToko
	err := c.ShouldBindUri(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Toko", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	tokoDetail, err := h.service.TokoServiceGetByID(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Toko", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail Toko", http.StatusOK, "success", formatter.FormatToko(tokoDetail))
	c.JSON(http.StatusOK, response)
}

func (h *tokoHandler) GetTokoByUserID(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	tokoDetail, err := h.service.TokoServiceGetByUserID(userID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Toko", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Detail Toko", http.StatusOK, "success", formatter.FormatToko(tokoDetail))
	c.JSON(http.StatusOK, response)

}

func (h *tokoHandler) GetTokos(c *gin.Context) {
	tokos, err := h.service.TokoServiceGetAll()
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Tokos", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of Tokos", http.StatusOK, "success", formatter.FormatTokos(tokos))
	c.JSON(http.StatusOK, response)
}

func (h *tokoHandler) CreateToko(c *gin.Context) {

	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	var input input.TokoInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Toko failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	input.UserID = userID
	if currentUser.Role == "mitra" {
		newToko, err := h.service.TokoServiceCreate(input)
		if err != nil {
			pc, fn, line, _ := runtime.Caller(0)
			errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
			helper.PrintErr(err, errornya)

			response := helper.ApiResponse("Create Toko failed", http.StatusOK, "error", nil)
			c.JSON(http.StatusOK, response)
			return
		}
		response := helper.ApiResponse("Successfully Create Toko", http.StatusOK, "success", formatter.FormatToko(newToko))
		c.JSON(http.StatusOK, response)
	} else {
		if err != nil {
			pc, fn, line, _ := runtime.Caller(0)
			errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
			helper.PrintErr(err, errornya)

			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}
	}
}
func (h *tokoHandler) UpdateToko(c *gin.Context) {
	var inputID input.InputIDToko
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Tokos", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	var inputData input.TokoInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Toko failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID

	isExists, err := h.service.TokoServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Tokos", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	if isExists.UserID != userID {
		response := helper.ApiResponse("Not Allowed to update", http.StatusUnauthorized, "error", nil)
		c.JSON(http.StatusUnauthorized, response)
		return
	}
	inputData.UserID = userID
	updatedToko, err := h.service.TokoServiceUpdate(inputID, inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Tokos", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Toko", http.StatusOK, "success", formatter.FormatToko(updatedToko))
	c.JSON(http.StatusOK, response)
}
func (h *tokoHandler) DeleteToko(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDToko
	inputID.ID = id
	_, err := h.service.TokoServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Tokos", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.TokoServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Tokos", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Toko", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
