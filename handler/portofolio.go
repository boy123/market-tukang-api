package handler

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/formatter"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/service"
	"github.com/gin-gonic/gin"
)

type portofolioHandler struct {
	service service.PortofolioService
}

func NewPortofolioHandler(service service.PortofolioService) *portofolioHandler {
	return &portofolioHandler{service}
}
func (h *portofolioHandler) GetPortofolio(c *gin.Context) {
	var input input.InputIDPortofolio
	err := c.ShouldBindUri(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolio", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	portofolioDetail, err := h.service.PortofolioServiceGetByID(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolio", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	response := helper.ApiResponse("Detail Portofolio", http.StatusOK, "success", formatter.FormatPortofolio(portofolioDetail))
	c.JSON(http.StatusOK, response)
}

func (h *portofolioHandler) GetPortofolios(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	portofolios, err := h.service.PortofolioServiceGetAll(userID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("List of Portofolios", http.StatusOK, "success", formatter.FormatPortofolios(portofolios))
	c.JSON(http.StatusOK, response)
}

func (h *portofolioHandler) CreatePortofolio(c *gin.Context) {
	var input input.PortofolioInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Portofolio failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	input.UserID = userID
	newPortofolio, err := h.service.PortofolioServiceCreate(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Create Portofolio failed", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Portofolio", http.StatusOK, "success", formatter.FormatPortofolio(newPortofolio))
	c.JSON(http.StatusOK, response)
}
func (h *portofolioHandler) UpdatePortofolio(c *gin.Context) {
	var inputID input.InputIDPortofolio
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	var inputData input.PortofolioInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Portofolio failed", http.StatusOK, "error", errorMessage)
		c.JSON(http.StatusOK, response)
		return
	}

	portofolio, err := h.service.PortofolioServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	if portofolio.UserID != userID {
		response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		return
	}
	inputData.UserID = userID
	updatedPortofolio, err := h.service.PortofolioServiceUpdate(inputID, inputData)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Portofolio", http.StatusOK, "success", formatter.FormatPortofolio(updatedPortofolio))
	c.JSON(http.StatusOK, response)
}
func (h *portofolioHandler) DeletePortofolio(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDPortofolio
	inputID.ID = id
	_, err := h.service.PortofolioServiceGetByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.PortofolioServiceDeleteByID(inputID)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Portofolio", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}

func (h *portofolioHandler) CreateDetailImagePortofolio(c *gin.Context) {
	var input input.PortofolioImageInput
	currentUser := c.MustGet("currentUser").(entity.MasterUser)
	userID := currentUser.ID
	err := c.ShouldBindJSON(&input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to get Portofolios", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	input.User.ID = userID
	input.User.Username = currentUser.Username
	portofolioImage, err := h.service.PortofolioServiceCreateImages(input)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse(err.Error(), http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	response := helper.ApiResponse("Foto berhasil ditambahkan", http.StatusOK, "success", formatter.FormatPortofolioImage(portofolioImage))
	c.JSON(http.StatusOK, response)

}

func (h *portofolioHandler) DeleteImage(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	isFound, err := h.service.PortofolioServiceFindImageByID(id)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Image not found", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	if !isFound {
		response := helper.ApiResponse("Image not found", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}
	_, err = h.service.PortofolioServiceDeleteImageByID(id)
	if err != nil {
		endpoint := c.Request.Host + c.Request.URL.Path
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, fmt.Sprintf("%s endpoint => %s", errornya, endpoint))

		response := helper.ApiResponse("Failed to delete image", http.StatusOK, "error", nil)
		c.JSON(http.StatusOK, response)
		return
	}

	response := helper.ApiResponse("Delete Image Success", http.StatusOK, "success", gin.H{"deleted": true})
	c.JSON(http.StatusOK, response)

}

// func (h *portofolioHandler) DeleteImage(c *gin.Context) {
// 	param := c.Param("id")
// 	id, _ := strconv.Atoi(param)
// 	var inputID input.InputIDPortofolio
// 	inputID.ID = id

// }
