package es

import (
	"context"

	"github.com/olivere/elastic/v7"
)

type ElasticSearch interface {
	Connect() (*elastic.Client, error)
	MakeIndex(client *elastic.Client, indexName string)
}

type elasticSearch struct {
	elasticClient *elastic.Client
}

func NewElasticSearch(elasticClient *elastic.Client) *elasticSearch {
	return &elasticSearch{elasticClient}
}

func (c *elasticSearch) MakeIndex(indexName string) (bool, error) {
	_, err := c.elasticClient.CreateIndex(indexName).Do(context.Background())
	if err != nil {
		return false, err
	}
	return true, nil
}
