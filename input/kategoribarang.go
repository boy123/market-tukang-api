package input

type InputIDKategoriBarang struct {
	ID int `uri:"id" binding:"required"`
}

type KategoriBarangInput struct {
	Kategori string `json:"kategori"`
}

//Generated by Micagen at 21 Desember 2021
