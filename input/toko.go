package input

type InputIDToko struct {
	ID int `uri:"id" binding:"required"`
}

type TokoInput struct {
	UserID          int    `json:"user_id"`
	NamaToko        string `json:"nama_toko" binding:"required"`
	Alamat          string `json:"alamat" binding:"required"`
	NoTelp          string `json:"no_telp" binding:"required"`
	HariOperasional string `json:"hari_operasional" binding:"required"`
	JamOperasional  string `json:"jam_operasional" binding:"required"`
	Status          string `json:"status" binding:"required"`
}

//Generated by Micagen at 21 Desember 2021
