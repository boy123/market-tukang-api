package input

type InputIDMasterUser struct {
	ID int `uri:"id" json:"id" binding:"required"`
}

type MasterUserInput struct {
	Username       string  `json:"username"`
	NamaLengkap    string  `json:"nama_lengkap"`
	Password       string  `json:"password" binding:"required"`
	Email          string  `json:"email"`
	PhoneNumber    string  `json:"phone_number"`
	Role           string  `json:"role"`
	Status         int     `json:"status"`
	TokenFCM       string  `json:"token_fcm"`
	ProfilePicture string  `json:"profile_picture"`
	AlasanNonAktif string  `json:"alasan_non_aktif"`
	SaldoKoin      int     `json:"saldo_koin"`
	Badge          string  `json:"badge"`
	Verified       int     `json:"verified"`
	Latitude       float64 `json:"latitude"`
	Longitude      float64 `json:"longitude"`
}

type MasterUserInputElastic struct {
	ID             int     `json:"id"`
	Username       string  `json:"username"`
	NamaLengkap    string  `json:"nama_lengkap"`
	Password       string  `json:"password" binding:"required"`
	Email          string  `json:"email"`
	PhoneNumber    string  `json:"phone_number"`
	Role           string  `json:"role"`
	Status         int     `json:"status"`
	TokenFCM       string  `json:"token_fcm"`
	ProfilePicture string  `json:"profile_picture"`
	AlasanNonAktif string  `json:"alasan_non_aktif"`
	SaldoKoin      int     `json:"saldo_koin"`
	Badge          string  `json:"badge"`
	Verified       int     `json:"verified"`
	Latitude       float64 `json:"latitude"`
	Longitude      float64 `json:"longitude"`
}

type UserLocationInput struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

//Generated by Micagen at 17 Desember 2021
