package input

type InputIDSlider struct {
	ID int `uri:"id" binding:"required"`
}

type SliderInput struct {
	Image  string `json:"image" form:"image,omitempty"`
	Active string `json:"active" form:"active"`
}

//Generated by Micagen at 27 Desember 2021
