package input

type InputIDJenisPekerjaan struct {
	ID int `uri:"id" binding:"required"`
}

type JenisPekerjaanInput struct {
	JenisPekerjaan string `json:"jenis_pekerjaan"`
}

//Generated by Micagen at 21 Desember 2021
