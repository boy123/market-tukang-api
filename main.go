package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/ekokurniadi/marjasa-backend/handler"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/middleware"
	"github.com/ekokurniadi/marjasa-backend/repository"
	"github.com/ekokurniadi/marjasa-backend/service"

	firebase "firebase.google.com/go"
	"github.com/ekokurniadi/micagen"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"google.golang.org/api/option"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	// env := godotenv.Load()
	// if env != nil {
	// 	log.Fatal("Error on loading .env file")
	// 	return
	// }

	host := os.Getenv("HOST")
	userHost := os.Getenv("USER_HOST")
	userPass := os.Getenv("USER_PASS")
	databaseName := os.Getenv("DATABASE_NAME")
	databasePort := os.Getenv("DATABASE_PORT")

	dsn := "host=" + host + " user=" + userHost + " password=" + userPass + " dbname=" + databaseName + " port=" + databasePort + " sslmode=require TimeZone=Asia/Jakarta"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
		log.Fatal(err)
		return
	}

	/*
		Initial for firestore database
	*/
	ctx := context.Background()
	sa := option.WithCredentialsFile(os.Getenv("CREDENTIAL_JSON"))
	app, errno := firebase.NewApp(ctx, nil, sa)
	if errno != nil {
		fmt.Println(errno)
	}

	_ = micagen.Micagen{}

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(CORSMiddleware())

	cookieStore := cookie.NewStore([]byte(middleware.SECRET_KEY))
	router.Use(sessions.Sessions("markettukang", cookieStore))

	router.Static("slider", "./images/slider/")
	router.Static("artikel", "./images/artikel/")
	router.Static("profile_picture", "./images/profile_picture/")
	router.Static("portofolio", "./images/portofolio/")
	router.Static("ktp", "./images/ktp/")
	router.Static("barang", "./images/barang/")
	api := router.Group("/api/v1")
	authService := middleware.NewService()

	userRepository := repository.NewMasterUserRepository(db)
	userService := service.NewMasterUserService(userRepository)
	userHandler := handler.NewMasterUserHandler(userService, authService)

	biodataRepository := repository.NewMasterBiodataRepository(db)
	biodataService := service.NewMasterBiodataService(biodataRepository)
	biodataHandler := handler.NewMasterBiodataHandler(biodataService)

	portofolioRepository := repository.NewPortofolioRepository(db)
	portofolioService := service.NewPortofolioService(portofolioRepository)
	portofolioHandler := handler.NewPortofolioHandler(portofolioService)

	jenisPekerjaanRepository := repository.NewJenisPekerjaanRepository(db)
	jenisPekerjaanService := service.NewJenisPekerjaanService(jenisPekerjaanRepository)
	jenisPekerjaanHandler := handler.NewJenisPekerjaanHandler(jenisPekerjaanService)

	kategoriBarangRepository := repository.NewKategoriBarangRepository(db)
	kategoriBarangService := service.NewKategoriBarangService(kategoriBarangRepository)
	kategoriBarangHandler := handler.NewKategoriBarangHandler(kategoriBarangService)

	mitraRepository := repository.NewTokoRepository(db)
	mitraService := service.NewTokoService(mitraRepository)
	mitraHandler := handler.NewTokoHandler(mitraService)

	barangRepository := repository.NewBarangRepository(db)
	barangService := service.NewBarangService(barangRepository)
	barangHandler := handler.NewBarangHandler(barangService, mitraService)

	reviewRepository := repository.NewReviewRepository(db)
	reviewService := service.NewReviewService(reviewRepository)
	reviewHandler := handler.NewReviewHandler(reviewService)

	projectRepository := repository.NewProjectRepository(db)
	projectService := service.NewProjectService(projectRepository)
	proposalProjectRepository := repository.NewProposalProjectRepository(db, *userRepository)
	proposalProjectService := service.NewProposalProjectService(proposalProjectRepository)
	projectHandler := handler.NewProjectHandler(projectService, proposalProjectService)
	proposalProjectHandler := handler.NewProposalProjectHandler(proposalProjectService, projectRepository)

	sliderRepository := repository.NewSliderRepository(db)
	sliderService := service.NewSliderService(sliderRepository)
	sliderHandler := handler.NewSliderHandler(sliderService, sliderRepository)

	artikelRepository := repository.NewArtikelRepository(db)
	artikelService := service.NewArtikelService(artikelRepository)
	artikelHandler := handler.NewArtikelHandler(artikelService, artikelRepository)

	firebaseRepository := repository.NewErrorLogRepository(app)
	firebaseService := service.NewErrorLogService(firebaseRepository)
	firebaseHandler := handler.NewErrorLogHandler(firebaseService)

	kategoriPekerjaanRepository := repository.NewKategoriPekerjaanRepository(db)
	kategoriPekerjaanService := service.NewKategoriPekerjaanService(kategoriPekerjaanRepository)
	kategoriPekerjaanHandler := handler.NewKategoriPekerjaanHandler(kategoriPekerjaanService)

	//endpoint login & register
	api.POST("/auth/register", userHandler.Register)
	api.POST("/auth/login", userHandler.Login)
	api.POST("/auth/login_admin", userHandler.LoginAdmin)
	// api.OPTIONS("/auth/users", userHandler.Option)
	api.GET("/auth/users", authMiddleware(authService, userService), userHandler.FetchUser)

	//endpoint re-new token
	api.POST("/auth/renew_token", userHandler.ReNewToken)

	//endpoint get user profiles
	api.POST("/users/profile", authMiddleware(authService, userService), userHandler.GetMasterUser)

	//endpoint biodata
	api.GET("/users/biodata", authMiddleware(authService, userService), biodataHandler.GetMasterBiodata)
	api.POST("/users/biodata", authMiddleware(authService, userService), biodataHandler.CreateMasterBiodata)

	//endpoint upload foto profile
	api.POST("/users/upload_profile_picture", authMiddleware(authService, userService), userHandler.UploadPhotoProfile)

	//endpoint update lokasi user
	api.POST("/users/update_location", authMiddleware(authService, userService), userHandler.UpdateLokasi)

	//endpoint portofolio
	api.GET("/users/portofolio", authMiddleware(authService, userService), portofolioHandler.GetPortofolios)
	api.GET("/users/portofolio/:id", authMiddleware(authService, userService), portofolioHandler.GetPortofolio)
	api.POST("/users/portofolio", authMiddleware(authService, userService), portofolioHandler.CreatePortofolio)
	api.PUT("/users/portofolio/:id", authMiddleware(authService, userService), portofolioHandler.UpdatePortofolio)
	api.DELETE("/users/portofolio/:id", authMiddleware(authService, userService), portofolioHandler.DeletePortofolio)

	//endpoint add image portofolio
	api.POST("/portofolio/images", authMiddleware(authService, userService), portofolioHandler.CreateDetailImagePortofolio)
	api.DELETE("/portofolio/images/:id", authMiddleware(authService, userService), portofolioHandler.DeleteImage)

	//endpoint jenis pekerjaan
	api.GET("/master/jenis_pekerjaan", jenisPekerjaanHandler.GetJenisPekerjaans)
	api.GET("/master/jenis_pekerjaan/:id", jenisPekerjaanHandler.GetJenisPekerjaan)
	api.POST("/master/jenis_pekerjaan", jenisPekerjaanHandler.CreateJenisPekerjaan)
	api.PUT("/master/jenis_pekerjaan/:id", jenisPekerjaanHandler.UpdateJenisPekerjaan)
	api.DELETE("/master/jenis_pekerjaan/:id", jenisPekerjaanHandler.DeleteJenisPekerjaan)

	//endpoint kategori barang
	api.GET("/master/kategori_barang", kategoriBarangHandler.GetKategoriBarangs)
	api.GET("/master/kategori_barang/:id", kategoriBarangHandler.GetKategoriBarang)
	api.POST("/master/kategori_barang", kategoriBarangHandler.CreateKategoriBarang)
	api.PUT("/master/kategori_barang/:id", kategoriBarangHandler.UpdateKategoriBarang)
	api.DELETE("/master/kategori_barang/:id", kategoriBarangHandler.DeleteKategoriBarang)

	//endpoint mitra barang
	api.GET("/mitra/barang", authMiddleware(authService, userService), barangHandler.GetBarangs)
	api.GET("/mitra/barang/:id", authMiddleware(authService, userService), barangHandler.GetBarang)
	api.POST("/mitra/barang/", authMiddleware(authService, userService), barangHandler.CreateBarang)
	api.PUT("/mitra/barang/:id", authMiddleware(authService, userService), barangHandler.UpdateBarang)
	api.DELETE("/mitra/barang/:id", authMiddleware(authService, userService), barangHandler.DeleteBarang)

	//endpoint mitra
	api.GET("/mitra", authMiddleware(authService, userService), mitraHandler.GetTokos)
	api.GET("/mitra/:id", authMiddleware(authService, userService), mitraHandler.GetToko)
	api.POST("/mitra", authMiddleware(authService, userService), mitraHandler.CreateToko)
	api.PUT("/mitra/:id", authMiddleware(authService, userService), mitraHandler.UpdateToko)
	api.DELETE("/mitra/:id", authMiddleware(authService, userService), mitraHandler.DeleteToko)
	api.GET("/mitra/owned", authMiddleware(authService, userService), mitraHandler.GetTokoByUserID)

	//endpoint review project
	api.GET("/project/:id/review", authMiddleware(authService, userService), reviewHandler.GetReviews)
	api.GET("/project/review/:id", authMiddleware(authService, userService), reviewHandler.GetReview)
	api.POST("/project/review", authMiddleware(authService, userService), reviewHandler.CreateReview)
	api.PUT("/project/review/:id", authMiddleware(authService, userService), reviewHandler.UpdateReview)
	api.DELETE("/project/review/:id", authMiddleware(authService, userService), reviewHandler.DeleteReview)

	//endpoint project
	api.GET("/project", authMiddleware(authService, userService), projectHandler.GetProjects)
	api.GET("/project/:id", authMiddleware(authService, userService), projectHandler.GetProject)
	api.POST("/project", authMiddleware(authService, userService), projectHandler.CreateProject)
	api.PUT("/project/:id", authMiddleware(authService, userService), projectHandler.UpdateProject)
	api.DELETE("/project/:id", authMiddleware(authService, userService), projectHandler.DeleteProject)
	api.POST("/project/:id/invite", authMiddleware(authService, userService), projectHandler.InviteTukang)

	//endpoint proposal project
	api.GET("/project/:id/proposal", authMiddleware(authService, userService), proposalProjectHandler.GetProposalProjects)
	api.POST("/project/submit_proposal", authMiddleware(authService, userService), proposalProjectHandler.CreateProposalProject)

	//endpoint slider
	api.GET("/sliders", authMiddleware(authService, userService), sliderHandler.GetSliders)
	api.GET("/sliders/:id", authMiddleware(authService, userService), sliderHandler.GetSlider)
	api.POST("/sliders/:id", authMiddleware(authService, userService), sliderHandler.UpdateSlider)
	api.POST("/sliders", authMiddleware(authService, userService), sliderHandler.CreateSlider)
	api.DELETE("/sliders/:id", authMiddleware(authService, userService), sliderHandler.DeleteSlider)
	api.GET("/sliders_fetch", authMiddleware(authService, userService), sliderHandler.FetchData)

	//endpoint artikel
	api.POST("/artikel", authMiddleware(authService, userService), artikelHandler.CreateArtikel)
	api.POST("/artikel/:id", authMiddleware(authService, userService), artikelHandler.UpdateArtikel)
	api.GET("/artikel/:id", authMiddleware(authService, userService), artikelHandler.GetArtikel)
	api.GET("/artikel_fetch", authMiddleware(authService, userService), artikelHandler.FetchData)
	api.GET("/artikel", authMiddleware(authService, userService), artikelHandler.GetArtikels)
	api.DELETE("/artikel/:id", authMiddleware(authService, userService), artikelHandler.DeleteArtikel)

	//endpoint kategori pekerjaan
	api.POST("/kategori_pekerjaan", authMiddleware(authService, userService), kategoriPekerjaanHandler.CreateKategoriPekerjaan)
	api.PUT("/kategori_pekerjaan/:id", authMiddleware(authService, userService), kategoriPekerjaanHandler.UpdateKategoriPekerjaan)
	api.GET("/kategori_pekerjaan", authMiddleware(authService, userService), kategoriPekerjaanHandler.GetKategoriPekerjaans)
	api.GET("/kategori_pekerjaan/:id", authMiddleware(authService, userService), kategoriPekerjaanHandler.GetKategoriPekerjaan)
	api.DELETE("/kategori_pekerjaan/:id", authMiddleware(authService, userService), kategoriPekerjaanHandler.DeleteKategoriPekerjaan)

	api.GET("/logs", firebaseHandler.GetAll)

	//endpoint elastic search
	// api.POST("/users/search", userHandler.SearchUserOnElastic)

	fmt.Println("Server connected")
	router.Run()

}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Allow-Methods", "POST,HEAD,PATCH, OPTIONS, GET, PUT,DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func authMiddleware(authService middleware.Service, userService service.MasterUserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			response := helper.ApiResponse("UnAuthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		arrayToken := strings.Split(authHeader, " ")

		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			response := helper.ApiResponse(strings.Title(err.Error()), http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			response := helper.ApiResponse("UnAuthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		userID := int(claim["user_id"].(float64))
		var input input.InputIDMasterUser
		input.ID = userID
		user, err := userService.MasterUserServiceGetByID(input)

		if err != nil {
			response := helper.ApiResponse("UnAuthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		c.Set("currentUser", user)

	}

}
