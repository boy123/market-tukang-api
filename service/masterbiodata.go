package service

import (
	"fmt"
	"runtime"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/repository"
)

type MasterBiodataService interface {
	MasterBiodataServiceGetAll() ([]entity.MasterBiodata, error)
	MasterBiodataServiceGetByID(inputID input.InputIDMasterBiodata) (entity.MasterBiodata, error)
	MasterBiodataServiceGetByUserID(userID int) (entity.MasterBiodata, error)
	MasterBiodataServiceCreate(input input.MasterBiodataInput) (entity.MasterBiodata, error)
	MasterBiodataServiceUpdate(inputID input.InputIDMasterBiodata, inputData input.MasterBiodataInput) (entity.MasterBiodata, error)
	MasterBiodataServiceDeleteByID(inputID input.InputIDMasterBiodata) (bool, error)
}
type masterbiodataService struct {
	repository repository.MasterBiodataRepository
}

func NewMasterBiodataService(repository repository.MasterBiodataRepository) *masterbiodataService {
	return &masterbiodataService{repository}
}
func (s *masterbiodataService) MasterBiodataServiceCreate(input input.MasterBiodataInput) (entity.MasterBiodata, error) {
	masterbiodata := entity.MasterBiodata{}
	masterbiodata.UserID = input.UserID
	masterbiodata.DeskripsiProfil = input.DeskripsiProfil
	masterbiodata.Alamat = input.Alamat
	masterbiodata.JenisKelamin = input.JenisKelamin
	masterbiodata.NikKTP = input.NikKTP
	fileKtp := fmt.Sprintf("%d-%s-%s", input.UserID, input.User.Username, input.ImageKTP.Filename)
	masterbiodata.ImageKTP = fileKtp
	masterbiodata.Skill = input.Skill
	masterbiodata.RatePerDays = input.RatePerDays
	masterbiodata.ActiveWork = input.ActiveWork
	masterbiodata.JamKerja = input.JamKerja
	newMasterBiodata, err := s.repository.SaveMasterBiodata(masterbiodata)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return newMasterBiodata, err
	}
	return newMasterBiodata, nil
}
func (s *masterbiodataService) MasterBiodataServiceUpdate(inputID input.InputIDMasterBiodata, inputData input.MasterBiodataInput) (entity.MasterBiodata, error) {
	masterbiodata, err := s.repository.FindByIDMasterBiodata(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masterbiodata, err
	}
	masterbiodata.UserID = inputData.UserID
	masterbiodata.DeskripsiProfil = inputData.DeskripsiProfil
	masterbiodata.Alamat = inputData.Alamat
	masterbiodata.JenisKelamin = inputData.JenisKelamin
	masterbiodata.NikKTP = inputData.NikKTP
	fileKtp := fmt.Sprintf("%d-%s-%s", inputData.UserID, inputData.User.Username, inputData.ImageKTP.Filename)
	masterbiodata.ImageKTP = fileKtp
	masterbiodata.Skill = inputData.Skill
	masterbiodata.RatePerDays = inputData.RatePerDays
	masterbiodata.ActiveWork = inputData.ActiveWork
	masterbiodata.JamKerja = inputData.JamKerja

	updatedMasterBiodata, err := s.repository.UpdateMasterBiodata(masterbiodata)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return updatedMasterBiodata, err
	}
	return updatedMasterBiodata, nil
}
func (s *masterbiodataService) MasterBiodataServiceGetByID(inputID input.InputIDMasterBiodata) (entity.MasterBiodata, error) {
	masterbiodata, err := s.repository.FindByIDMasterBiodata(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masterbiodata, err
	}
	return masterbiodata, nil
}
func (s *masterbiodataService) MasterBiodataServiceGetAll() ([]entity.MasterBiodata, error) {
	masterbiodatas, err := s.repository.FindAllMasterBiodata()
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masterbiodatas, err
	}
	return masterbiodatas, nil
}
func (s *masterbiodataService) MasterBiodataServiceDeleteByID(inputID input.InputIDMasterBiodata) (bool, error) {
	_, err := s.repository.FindByIDMasterBiodata(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	_, err = s.repository.DeleteByIDMasterBiodata(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	return true, nil
}

func (s *masterbiodataService) MasterBiodataServiceGetByUserID(userID int) (entity.MasterBiodata, error) {
	biodataUser, err := s.repository.FindByUserID(userID)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return biodataUser, err
	}
	return biodataUser, nil
}

//Generated by Micagen at 17 Desember 2021
