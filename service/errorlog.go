package service

import (
	"fmt"
	"runtime"

	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/repository"
)

type ErrorLogService interface {
	GetAll(search string, page, size int) (interface{}, error)
}

type errorLogService struct {
	repository repository.ErrorLogRepository
}

func NewErrorLogService(repository repository.ErrorLogRepository) *errorLogService {
	return &errorLogService{repository}
}

func (s *errorLogService) GetAll(search string, page, size int) (interface{}, error) {
	searchQuery := search
	pages := (page * size)
	length := size

	fetchData, err := s.repository.GetAll(searchQuery, pages, length)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)
	}
	return fetchData, nil
}
