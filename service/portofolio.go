package service

import (
	"fmt"
	"runtime"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/repository"
)

type PortofolioService interface {
	PortofolioServiceGetAll(userID int) ([]entity.Portofolio, error)
	PortofolioServiceGetByID(inputID input.InputIDPortofolio) (entity.Portofolio, error)
	PortofolioServiceCreate(input input.PortofolioInput) (entity.Portofolio, error)
	PortofolioServiceUpdate(inputID input.InputIDPortofolio, inputData input.PortofolioInput) (entity.Portofolio, error)
	PortofolioServiceDeleteByID(inputID input.InputIDPortofolio) (bool, error)
	PortofolioServiceCreateImages(input input.PortofolioImageInput) (entity.PortofolioImage, error)
	PortofolioServiceDeleteImageByID(ID int) (bool, error)
	PortofolioServiceFindImageByID(ID int) (bool, error)
}
type portofolioService struct {
	repository repository.PortofolioRepository
}

func NewPortofolioService(repository repository.PortofolioRepository) *portofolioService {
	return &portofolioService{repository}
}
func (s *portofolioService) PortofolioServiceCreate(input input.PortofolioInput) (entity.Portofolio, error) {
	portofolio := entity.Portofolio{}
	portofolio.UserID = input.UserID
	portofolio.Title = input.Title
	portofolio.Deskripsi = input.Deskripsi
	portofolio.Status = input.Status
	portofolio.Kategori = input.Kategori
	portofolio.Tarif = input.Tarif
	newPortofolio, err := s.repository.SavePortofolio(portofolio)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return newPortofolio, err
	}
	return newPortofolio, nil
}
func (s *portofolioService) PortofolioServiceUpdate(inputID input.InputIDPortofolio, inputData input.PortofolioInput) (entity.Portofolio, error) {
	portofolio, err := s.repository.FindByIDPortofolio(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return portofolio, err
	}
	portofolio.UserID = inputData.UserID
	portofolio.Title = inputData.Title
	portofolio.Deskripsi = inputData.Deskripsi
	portofolio.Status = inputData.Status
	portofolio.Kategori = inputData.Kategori
	portofolio.Tarif = inputData.Tarif
	updatedPortofolio, err := s.repository.UpdatePortofolio(portofolio)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return updatedPortofolio, err
	}
	return updatedPortofolio, nil
}
func (s *portofolioService) PortofolioServiceGetByID(inputID input.InputIDPortofolio) (entity.Portofolio, error) {
	portofolio, err := s.repository.FindByIDPortofolio(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return portofolio, err
	}
	return portofolio, nil
}
func (s *portofolioService) PortofolioServiceGetAll(userID int) ([]entity.Portofolio, error) {
	portofolios, err := s.repository.FindAllPortofolio(userID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return portofolios, err
	}

	return portofolios, nil
}
func (s *portofolioService) PortofolioServiceDeleteByID(inputID input.InputIDPortofolio) (bool, error) {
	_, err := s.repository.FindByIDPortofolio(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	_, err = s.repository.DeleteByIDPortofolio(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}

	return true, nil
}

func (s *portofolioService) PortofolioServiceCreateImages(input input.PortofolioImageInput) (entity.PortofolioImage, error) {

	if input.IsPrimary == 1 {
		_, err := s.repository.MarkAllImagesAsNonPrimary(input.PortofolioID)
		if err != nil {
			pc, fn, line, _ := runtime.Caller(0)
			errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
			helper.PrintErr(err, errornya)

			return entity.PortofolioImage{}, err
		}
	}

	portofolioImage := entity.PortofolioImage{}
	portofolioImage.PortofolioID = input.PortofolioID
	portofolioImage.IsPrimary = input.IsPrimary
	portofolioImage.Image = input.Image
	portofolioImage.Deskripsi = input.Deskripsi

	newPortofolioImage, err := s.repository.CreateImage(portofolioImage)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return newPortofolioImage, err
	}

	return newPortofolioImage, nil

}

func (s *portofolioService) PortofolioServiceDeleteImageByID(ID int) (bool, error) {
	image, err := s.repository.DeleteImageByID(ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return image, err
	}
	return image, nil
}
func (s *portofolioService) PortofolioServiceFindImageByID(ID int) (bool, error) {
	image, err := s.repository.FindImageByID(ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return image, err
	}
	return image, nil
}

//Generated by Micagen at 21 Desember 2021
