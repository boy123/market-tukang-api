package service

import (
	"fmt"
	"runtime"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/repository"
)

type ProjectService interface {
	ProjectServiceGetAll() ([]entity.Project, error)
	ProjectServiceGetByID(inputID input.InputIDProject) (entity.Project, error)
	ProjectServiceGetByIDWithListProposal(inputID input.InputIDProject) (entity.Project, error)
	ProjectServiceCreate(input input.ProjectInput) (entity.Project, error)
	ProjectServiceUpdate(inputID input.InputIDProject, inputData input.ProjectInput) (entity.Project, error)
	ProjectServiceDeleteByID(inputID input.InputIDProject) (bool, error)
	ProjectServiceGetAllByUser(userID int) ([]entity.Project, error)
}
type projectService struct {
	repository repository.ProjectRepository
}

func NewProjectService(repository repository.ProjectRepository) *projectService {
	return &projectService{repository}
}
func (s *projectService) ProjectServiceCreate(input input.ProjectInput) (entity.Project, error) {
	project := entity.Project{}
	project.UserID = input.UserIDClient
	projectNumber := ""
	userName := input.User.Username
	number, _ := s.repository.GetAutoNumber(input.UserIDClient, userName)
	projectNumber = number
	project.NoProject = projectNumber
	project.Judul = input.Judul
	project.Deskripsi = input.Deskripsi
	project.Lokasi = input.Lokasi
	project.TargetSelesai = input.TargetSelesai
	project.IDJenisPekerjaan = input.IDJenisPekerjaan
	project.EstimasiBudget = input.EstimasiBudget
	project.Status = input.Status
	newProject, err := s.repository.SaveProject(project)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return newProject, err
	}
	return newProject, nil
}
func (s *projectService) ProjectServiceUpdate(inputID input.InputIDProject, inputData input.ProjectInput) (entity.Project, error) {
	project, err := s.repository.FindByIDProject(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return project, err
	}
	project.UserID = inputData.UserIDClient
	project.Judul = inputData.Judul
	project.Deskripsi = inputData.Deskripsi
	project.Lokasi = inputData.Lokasi
	project.TargetSelesai = inputData.TargetSelesai
	project.IDJenisPekerjaan = inputData.IDJenisPekerjaan
	project.EstimasiBudget = inputData.EstimasiBudget
	project.Status = inputData.Status

	updatedProject, err := s.repository.UpdateProject(project)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return updatedProject, err
	}
	return updatedProject, nil
}
func (s *projectService) ProjectServiceGetByID(inputID input.InputIDProject) (entity.Project, error) {
	project, err := s.repository.FindByIDProject(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return project, err
	}
	return project, nil
}

func (s *projectService) ProjectServiceGetByIDWithListProposal(inputID input.InputIDProject) (entity.Project, error) {
	project, err := s.repository.FindByIDProject(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return project, err
	}
	return project, nil
}

func (s *projectService) ProjectServiceGetAll() ([]entity.Project, error) {
	projects, err := s.repository.FindAllProject()
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return projects, err
	}
	return projects, nil
}

func (s *projectService) ProjectServiceGetAllByUser(userID int) ([]entity.Project, error) {
	projects, err := s.repository.FindAllProjectByUser(userID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return projects, err
	}
	return projects, nil
}

func (s *projectService) ProjectServiceDeleteByID(inputID input.InputIDProject) (bool, error) {
	_, err := s.repository.FindByIDProject(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	_, err = s.repository.DeleteByIDProject(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	return true, nil
}

//Generated by Micagen at 21 Desember 2021
