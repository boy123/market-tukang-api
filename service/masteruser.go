package service

import (
	"errors"
	"fmt"
	"runtime"
	"strings"

	"github.com/ekokurniadi/marjasa-backend/entity"
	"github.com/ekokurniadi/marjasa-backend/helper"
	"github.com/ekokurniadi/marjasa-backend/input"
	"github.com/ekokurniadi/marjasa-backend/repository"
	"golang.org/x/crypto/bcrypt"
)

type MasterUserService interface {
	MasterUserServiceGetAll() ([]entity.MasterUser, error)
	MasterUserServiceGetByID(inputID input.InputIDMasterUser) (entity.MasterUser, error)
	MasterUserServiceCreate(input input.MasterUserInput) (entity.MasterUser, error)
	MasterUserServiceUpdate(inputID input.InputIDMasterUser, inputData input.MasterUserInput) (entity.MasterUser, error)
	MasterUserServiceDeleteByID(inputID input.InputIDMasterUser) (bool, error)
	MasterUserServiceGetByPhoneNumber(input input.MasterUserInput) (entity.MasterUser, error)
	MasterUserServiceLogin(input input.MasterUserInput) (entity.MasterUser, error)
	MasterUserServiceLoginAdmin(input input.MasterUserInput) (entity.MasterUser, error)
	MasterUserServiceUploadProfilePicture(ID int, imageFile string) (entity.MasterUser, error)
	MasterUserServiceUpdateToken(userID int, input input.MasterUserInput) (entity.MasterUser, error)
	MasterUserServiceUpdateLocation(userID int, input input.UserLocationInput) (entity.MasterUser, error)
	// MasterUserServiceInsertToIndexUser(userID int, input input.MasterUserInputElastic) (*elastic.IndexResponse, error)
	// SearchUser(query string) ([]entity.MasterUser, error)
}
type masteruserService struct {
	repository repository.MasterUserRepository
	// elasticClient *elastic.Client
}

func NewMasterUserService(repository repository.MasterUserRepository) *masteruserService {
	return &masteruserService{repository}
}
func (s *masteruserService) MasterUserServiceCreate(input input.MasterUserInput) (entity.MasterUser, error) {
	masteruser := entity.MasterUser{}
	masteruser.Username = helper.RandStringRunes(10)
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masteruser, err
	}
	masteruser.Password = string(passwordHash)
	masteruser.Email = input.Email
	firstCharacter := input.PhoneNumber[0:1]

	phoneNumber := ""
	if firstCharacter == "0" {
		phoneNumber = strings.Replace(input.PhoneNumber, firstCharacter, "62", 1)
	} else {
		phoneNumber = input.PhoneNumber
	}

	masteruser.PhoneNumber = phoneNumber

	masteruser.NamaLengkap = input.NamaLengkap
	masteruser.Role = input.Role
	masteruser.Status = input.Status
	masteruser.TokenFCM = input.TokenFCM
	masteruser.ProfilePicture = input.ProfilePicture
	masteruser.AlasanNonAktif = input.AlasanNonAktif
	masteruser.SaldoKoin = input.SaldoKoin
	masteruser.Badge = input.Badge
	masteruser.Verified = input.Verified
	newMasterUser, err := s.repository.SaveMasterUser(masteruser)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return newMasterUser, err
	}
	return newMasterUser, nil
}
func (s *masteruserService) MasterUserServiceUpdate(inputID input.InputIDMasterUser, inputData input.MasterUserInput) (entity.MasterUser, error) {
	masteruser, err := s.repository.FindByIDMasterUser(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masteruser, err
	}
	masteruser.Username = inputData.Username
	masteruser.NamaLengkap = inputData.NamaLengkap
	masteruser.Password = inputData.Password
	masteruser.Email = inputData.Email
	masteruser.PhoneNumber = inputData.PhoneNumber
	masteruser.Role = inputData.Role
	masteruser.Status = inputData.Status
	masteruser.TokenFCM = inputData.TokenFCM
	masteruser.ProfilePicture = inputData.ProfilePicture
	masteruser.AlasanNonAktif = inputData.AlasanNonAktif
	masteruser.SaldoKoin = inputData.SaldoKoin
	masteruser.Badge = inputData.Badge
	masteruser.Verified = inputData.Verified

	updatedMasterUser, err := s.repository.UpdateMasterUser(masteruser)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return updatedMasterUser, err
	}
	return updatedMasterUser, nil
}
func (s *masteruserService) MasterUserServiceUpdateToken(userID int, input input.MasterUserInput) (entity.MasterUser, error) {
	masteruser, err := s.repository.FindByIDMasterUser(userID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masteruser, err
	}
	masteruser.TokenFCM = input.TokenFCM
	updatedMasterUser, err := s.repository.UpdateMasterUser(masteruser)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return updatedMasterUser, err
	}
	return updatedMasterUser, nil
}

func (s *masteruserService) MasterUserServiceUpdateLocation(userID int, input input.UserLocationInput) (entity.MasterUser, error) {
	masterUser, err := s.repository.FindByIDMasterUser(userID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masterUser, err
	}

	masterUser.Latitude = input.Latitude
	masterUser.Longitude = input.Longitude
	updatedUser, err := s.repository.UpdateMasterUser(masterUser)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return updatedUser, err
	}
	return updatedUser, nil
}

func (s *masteruserService) MasterUserServiceGetByID(inputID input.InputIDMasterUser) (entity.MasterUser, error) {
	masteruser, err := s.repository.FindByIDMasterUser(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masteruser, err
	}
	return masteruser, nil
}

func (s *masteruserService) MasterUserServiceGetByPhoneNumber(input input.MasterUserInput) (entity.MasterUser, error) {
	firstCharacter := input.PhoneNumber[0:1]
	phoneNumber := ""
	if firstCharacter == "0" {
		phoneNumber = strings.Replace(input.PhoneNumber, firstCharacter, "62", 1)
	} else {
		phoneNumber = input.PhoneNumber
	}
	input.PhoneNumber = phoneNumber

	masteruser, err := s.repository.FindByPhoneNumberMasterUser(input)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masteruser, err
	}
	return masteruser, nil
}

func (s *masteruserService) MasterUserServiceGetAll() ([]entity.MasterUser, error) {
	masterusers, err := s.repository.FindAllMasterUser()
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return masterusers, err
	}
	return masterusers, nil
}
func (s *masteruserService) MasterUserServiceDeleteByID(inputID input.InputIDMasterUser) (bool, error) {
	_, err := s.repository.FindByIDMasterUser(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	_, err = s.repository.DeleteByIDMasterUser(inputID.ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return false, err
	}
	return true, nil
}

func (s *masteruserService) MasterUserServiceLogin(input input.MasterUserInput) (entity.MasterUser, error) {
	firstCharacter := input.PhoneNumber[0:1]
	phoneNumber := ""
	if firstCharacter == "0" {
		phoneNumber = strings.Replace(input.PhoneNumber, firstCharacter, "62", 1)
	} else {
		phoneNumber = input.PhoneNumber
	}
	input.PhoneNumber = phoneNumber
	password := input.Password
	user, err := s.repository.FindByPhoneNumberMasterUser(input)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return user, err
	}

	if user.ID == 0 {
		return user, errors.New("user tidak ditemukan, mohon periksa kembali nomor telepon atau password anda")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return user, errors.New("terjadi kesalahan, password dan user anda tidak cocok")
	}
	return user, nil
}
func (s *masteruserService) MasterUserServiceLoginAdmin(input input.MasterUserInput) (entity.MasterUser, error) {
	password := input.Password
	user, err := s.repository.FindByUsernameMasterUser(input)

	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return user, err
	}

	if user.ID == 0 {
		return user, errors.New("user tidak ditemukan, mohon periksa kembali username atau password anda")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return user, errors.New("terjadi kesalahan, password dan user anda tidak cocok")
	}
	return user, nil
}

func (s *masteruserService) MasterUserServiceUploadProfilePicture(ID int, imageFile string) (entity.MasterUser, error) {
	user, err := s.repository.FindByIDMasterUser(ID)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return user, err
	}

	user.ProfilePicture = imageFile

	updatedUser, err := s.repository.UploadPhotoProfile(user)
	if err != nil {
		pc, fn, line, _ := runtime.Caller(0)
		errornya := fmt.Sprintf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
		helper.PrintErr(err, errornya)

		return user, err
	}
	return updatedUser, nil
}

// func (s *masteruserService) MasterUserServiceInsertToIndexUser(userID int, input input.MasterUserInputElastic) (*elastic.IndexResponse, error) {
// 	id := strconv.Itoa(userID)
// 	input.ID = userID
// 	data, err := s.elasticClient.Index().Index("user_market_tukang").Id(id).BodyJson(&input).Refresh("true").Do(context.Background())
// 	if err != nil {
// 		return data, err
// 	}
// 	return data, nil
// }

// func (s *masteruserService) SearchUser(query string) ([]entity.MasterUser, error) {
// 	var users []entity.MasterUser
// 	q := elastic.NewMultiMatchQuery(query, "nama_lengkap", "email").Type("bool_prefix")
// 	index := "user_market_tukang"

// 	searchResult, err := s.elasticClient.Search().Index(index).Query(q).Do(context.Background())
// 	if err != nil {
// 		return nil, err
// 	}
// 	for _, hit := range searchResult.Hits.Hits {
// 		var user entity.MasterUser
// 		err := json.Unmarshal(hit.Source, &user)
// 		if err != nil {
// 			return nil, err
// 		}
// 		users = append(users, user)
// 	}

// 	return users, nil
// }

//Generated by Micagen at 17 Desember 2021
